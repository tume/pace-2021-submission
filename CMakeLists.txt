cmake_minimum_required(VERSION 3.5)
project(ClustedExact CXX)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(COMMON_SOURCES
        src/core/graph.cpp
        src/core/graph-components.cpp
        src/core/induced-costs.cpp
        src/core/conflict-triples.cpp
        src/core/edge-operations.cpp
        src/core/min-cut.cpp
        src/solver/improved-solver.cpp
        src/solver/kernel.cpp
        src/solver/pre-processing.cpp
        src/main.cpp
)
set(DEBUG_SOURCES src/util/stopwatch.cpp)

add_custom_target(tests)
add_custom_target(debug)

add_definitions(-O3 -march=native -mtune=native)

# Basic, "release", target. This is the default target that will be built
# when running make without any arguments
add_executable(clusted_exact ${COMMON_SOURCES})

# The testsuite target. Running make with the target 'test' does not create
# a cluster-editing program, but instead creates a testsuite-application that
# runs some unit tests on various parts of the code
add_executable(testsuite EXCLUDE_FROM_ALL ${COMMON_SOURCES} ${DEBUG_SOURCES})
add_dependencies(tests testsuite)
target_compile_definitions(testsuite PRIVATE -DDEBUG=1 -DRUN_TEST_SUITE=1)

# Debug target. Running make with target 'debug' creates an executable that
# can be used for solving cluster editing instances, but will output additional
# info during the search process and after finding a solution.
add_executable(clusted_exact_debug EXCLUDE_FROM_ALL ${COMMON_SOURCES} ${DEBUG_SOURCES})
add_dependencies(debug clusted_exact_debug)
target_compile_definitions(clusted_exact_debug PRIVATE -DDEBUG=1 -DBENCHMARK=1)
