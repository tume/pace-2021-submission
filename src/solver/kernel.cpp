#include "kernel.hpp"

#include <bitset>
#include <cmath>

#include "../core/edge-operations.hpp"
#include "../core/conflict-triples.hpp"
#include "../core/min-cut.hpp"

#include "../util/stopwatch.hpp"
#include "../util/logging.hpp"

bool Kernel::fullKernel(ImprovedSolver &solver, WeightedGraph &graph) {
    onStartOperation("kernel");

    int status;

    while (true) {
        if (solver.getRemainingCost() < 0) {
            onEndOperation();
            return false;
        }
        status = nonConflictRule(solver, graph);
        if (status == STATUS_GRAPH_MODIFIED) {
            continue;
        }

        status = Kernel::heavyEdgeRule(graph);
        if (status == STATUS_GRAPH_MODIFIED) {
            continue;
        }

        status = Kernel::quickInducedCostRule(solver, graph);

        if (status == STATUS_UNSOLVABLE_INSTANCE) {
            onEndOperation();
            return false;
        } else if (status == STATUS_GRAPH_MODIFIED) {
            continue;
        }

        if (status == STATUS_UNSOLVABLE_INSTANCE) {
            onEndOperation();
            return false;
        } else if (status == STATUS_GRAPH_MODIFIED) {
            continue;
        }

        if (solver.depth <= 8 || solver.depth % 5 == 0) {
            status = Kernel::almostCliqueRule(solver, graph);
            if (status == STATUS_GRAPH_MODIFIED) {
                continue;
            }
        }
        status = Kernel::similarNeighborhoodRule(solver, graph);
        if (status == STATUS_GRAPH_MODIFIED) {
            continue;
        }

        break;
    }

    status = lowerBoundRule(solver, graph);

    onEndOperation();
    return status != STATUS_UNSOLVABLE_INSTANCE;
}

int Kernel::lowerBoundRule(ImprovedSolver& solver, WeightedGraph &graph) {
    onStartOperation("lowerBoundRule");
    ConflictTriples::clawConflictBound(graph);
    if (solver.getRemainingCost() < graph.getTotalDisjointConflictLowerBound()) {
        onEndOperation();
        return STATUS_UNSOLVABLE_INSTANCE;
    }
    std::list<std::pair<int, int>> permanentEdges;
    const auto& vertices = graph.getVertices();
    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto v = *vit;
            if (graph.getWeight(u, v) < -INF_WEIGHT / 2) {
                continue;
            }
            if (solver.getRemainingCost() < graph.getTotalDisjointConflictLowerBound()
                + graph.getEdgeLowerBoundChange(u, v) + std::abs(graph.getWeight(u, v))) {
                permanentEdges.emplace_back(u, v);
            }
        }
    }
    std::vector<bool> mergedVertices = std::vector<bool>(graph.getMaxVertex() * (graph.getMaxVertex() - 1) / 2, false);
    for (auto& edge : permanentEdges) {
        if (mergedVertices[edge.first] || mergedVertices[edge.second]) {
            continue;
        }
        const auto u = std::min(edge.first, edge.second);
        const auto v = std::max(edge.first, edge.second);
        if (graph.getWeight(u, v) > 0) {
            EdgeOperations::merge(graph, u, v);
            mergedVertices[v] = true;
        } else {
            EdgeOperations::forbid(graph, u, v);
        }
    }
    onEndOperation();
    if (solver.getRemainingCost() >= 0) {
        return STATUS_GRAPH_MODIFIED;
    }
    return STATUS_UNSOLVABLE_INSTANCE;
}

int Kernel::quickInducedCostRule(ImprovedSolver &solver, WeightedGraph &graph) {
    onStartOperation("quickInducedCostRule");

    auto bestEdge = NO_BEST_EDGE;
    auto bestEdgeCost = 0;
    auto status = STATUS_NO_CHANGES;

    auto remainingCost = solver.getRemainingCost();
    const auto& vertices = graph.getVertices();

    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto v = *vit;

            const auto icp = graph.getMergeCost(u, v);
            const auto weight = graph.getWeight(u, v);
            const auto icf = graph.getForbidCost(u, v);

            if (icp > remainingCost && icf > remainingCost) {
                onEndOperation();
                return STATUS_UNSOLVABLE_INSTANCE;
            } else if (icp > remainingCost && weight >= -INF_WEIGHT / 2) {
                EdgeOperations::forbid(graph, u, v);
                status = STATUS_GRAPH_MODIFIED;
                remainingCost = solver.getRemainingCost();
                continue;
            } else if (icf > remainingCost) {
                EdgeOperations::merge(graph, u, v);
                onEndOperation();
                return STATUS_GRAPH_MODIFIED;
            }
            if (weight >= -INF_WEIGHT / 2 && std::max(icp, weight) > bestEdgeCost) {
                bestEdge = std::make_pair(u, v);
                bestEdgeCost = std::max(icp, weight);
            }
        }
    }
    solver.kernelBestEdge = std::move(bestEdge);
    onEndOperation();

    return status;
}

int Kernel::fullInducedCostRule(ImprovedSolver &solver, WeightedGraph &graph) {
    onStartOperation("fullInducedCostRule");
    const auto bestEdge = solver.kernelBestEdge;
    if (bestEdge == NO_BEST_EDGE || true) {
        onEndOperation();
        return STATUS_NO_CHANGES;
    }
    const auto& vertices = graph.getVertices();

    const auto u = bestEdge.first;
    const auto v = bestEdge.second;

    EdgeOperations::forbid(graph, u, v);
    ConflictTriples::clawConflictBound(graph);
    const auto afterForbidLowerBound = graph.getAccumulatedCost() + graph.getTotalDisjointConflictLowerBound();
    EdgeOperations::undo(graph);
    EdgeOperations::merge(graph, u, v);
    ConflictTriples::clawConflictBound(graph);
    const auto afterMergeLowerBound = graph.getAccumulatedCost() + graph.getTotalDisjointConflictLowerBound();
    EdgeOperations::undo(graph);

    if (afterForbidLowerBound > solver.maxCost && afterMergeLowerBound > solver.maxCost) {
        onEndOperation();
        return STATUS_UNSOLVABLE_INSTANCE;
    } else if (afterMergeLowerBound > solver.maxCost) {
        EdgeOperations::forbid(graph, u, v);
        onEndOperation();
        return STATUS_GRAPH_MODIFIED;
    } else if (afterForbidLowerBound > solver.maxCost) {
        EdgeOperations::merge(graph, u, v);
        onEndOperation();
        return STATUS_GRAPH_MODIFIED;
    }

    onEndOperation();
    return STATUS_NO_CHANGES;
}

int Kernel::heavyEdgeRule(WeightedGraph &graph) {
    onStartOperation("heavyEdgeRule");
    const auto& vertices = graph.getVertices();

    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto v = *vit;
            const auto weight = graph.getWeight(u, v);
            if (weight > -INF_WEIGHT / 2 && (-weight > graph.getVertexNeighborSum(u) || -weight > graph.getVertexNeighborSum(v))) {
                EdgeOperations::forbid(graph, u, v);
                onEndOperation();
                return STATUS_GRAPH_MODIFIED;
            } else if (weight > graph.getVertexAllEdgeSum(u) - weight || weight > graph.getVertexAllEdgeSum(v) - weight) {
                EdgeOperations::merge(graph, u, v);
                onEndOperation();
                return STATUS_GRAPH_MODIFIED;
            } else if (weight * 3 > graph.getVertexNeighborSum(u) + graph.getVertexNeighborSum(v)) {
                EdgeOperations::merge(graph, u, v);
                onEndOperation();
                return STATUS_GRAPH_MODIFIED;
            }
        }
    }
    onEndOperation();

    return STATUS_NO_CHANGES;
}

int Kernel::almostCliqueRule(ImprovedSolver &solver, WeightedGraph &graph) {
    onStartOperation("almostCliqueKernel");

    auto initialVertex = -1;
    auto initialVertexCost = -1;
    // Find initial vertex maximizing sum(weight(u, p)) for all p in V
    for (auto u : graph.getVertices()) {
        auto currentCost = 0;
        // TODO can we halve the runtime here
        for (auto v : graph.getVertices()) {
            if (u == v) {
                continue;
            }
            currentCost += std::abs(graph.getWeight(u, v));
        }
        if (currentCost > initialVertexCost) {
            initialVertex = u;
            initialVertexCost = currentCost;
        }
    }

    // TODO extract into class variable and a bitmap
    std::vector<bool> markedVertices = std::vector<bool>(graph.getMaxVertex(), false);
    std::list<int> vertices = std::list<int>();
    vertices.push_back(initialVertex);
    markedVertices[initialVertex] = true;

    for (auto iteration = 0; iteration < static_cast<int>(graph.getVertices().size()) - 2; ++iteration) {
        int bestVertexConnectivity = -1;
        int bestVertex = -1;

        for (auto u : graph.getVertices()) {
            if (markedVertices[u]) {
                continue;
            }

            auto currentConnectivity = 0;
            // TODO can we halve the runtime here?
            for (auto v : graph.getVertices()) {
                if (!markedVertices[v] || u == v) {
                    continue;
                }
                currentConnectivity += std::max(0, graph.getWeight(u, v));
            }
            if (currentConnectivity > bestVertexConnectivity) {
                bestVertexConnectivity = currentConnectivity;
                bestVertex = u;
            }
        }

        markedVertices[bestVertex] = true;
        vertices.push_back(bestVertex);

        auto isolationCost = 0;
        auto inGraphMergeCost = 0;
        for (auto u : vertices) {
            for (auto v : graph.getVertices()) {
                if (markedVertices[v] && v > u) {
                    isolationCost += std::max(0, -graph.getWeight(u, v));
                    inGraphMergeCost += std::max(0, -graph.getWeight(u, v));
                } else if (!markedVertices[v]) {
                    isolationCost += std::max(0, graph.getWeight(u, v));
                }
                // There is a forbidden edge that would be merged, can't do anything here
                if (isolationCost > INF_WEIGHT) {
                    onEndOperation();
                    return STATUS_NO_CHANGES;
                }
            }
        }

        const auto minCut = MinCut::calculateMinimumCut(graph, vertices);

        if (minCut >= isolationCost) {
            auto smallestVertex = -1;
            for (auto u : vertices) {
                if (smallestVertex == -1) {
                    smallestVertex = u;
                    continue;
                }
                if (u < smallestVertex) {
                   EdgeOperations::merge(graph, u, smallestVertex);
                   smallestVertex = u;
                } else {
                   EdgeOperations::merge(graph, smallestVertex, u);
                }
            }
            onEndOperation();
            return STATUS_GRAPH_MODIFIED;
        }
    }

    onEndOperation();
    return STATUS_NO_CHANGES;
}

int Kernel::nonConflictRule(ImprovedSolver &solver, WeightedGraph &graph) {
    onStartOperation("nonConflictRule");
    auto status = STATUS_NO_CHANGES;

    auto& edges = graph.getPotentialConflictFreeEdges();
    while (!edges.empty()) {
        const auto edge = edges.front();
        edges.pop_front();
        const auto u = edge.first;
        const auto v = edge.second;
        auto uExists = false;
        auto vExists = false;
        for (auto p : graph.getVertices()) {
            uExists = uExists || p == u;
            vExists = vExists || p == v;
        }
        if (!graph.isConflictFree(u, v) || !uExists || !vExists) {
            continue;
        }
        const auto weight = graph.getWeight(u, v);

        if (weight > 0) {
            EdgeOperations::merge(graph, u, v);
            onEndOperation();
            return STATUS_GRAPH_MODIFIED;
        } else if (weight < 0 && weight > -INF_WEIGHT / 2) {
            EdgeOperations::forbid(graph, u, v);
            // Forbidding a non-edge is not really that meaningful, no need to
            // restart kernel immediately. But after processing all edges, we
            // want to return the fact that modifications were made
            status = STATUS_GRAPH_MODIFIED;
        }
    }

    onEndOperation();
    return status;
}

int Kernel::similarNeighborhoodRule(ImprovedSolver& solver, WeightedGraph &graph) {
    onStartOperation("similarNeighborhoodRule");
    const auto& vertices = graph.getVertices();
    auto weightThreshold = 0;
    auto edges = 0;
    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto v = *vit;
            const auto uv = graph.getWeight(u, v);
            if (uv > 0) {
                weightThreshold += uv;
                ++edges;
            }
        }
    }
    edges = std::max(1, edges);
    weightThreshold = weightThreshold * 3 / 2 / edges;
    int found = 0;
    int notFound = 0;

    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto v = *vit;
            std::list<int> relevantVertices;
            auto deltaU = 0;
            auto deltaV = 0;
            auto totalSum = 0;
            const auto uv = graph.getWeight(u, v);
            bool impossible = uv < (std::floor(std::sqrt(std::max(0, solver.depth - 8)))) * weightThreshold;
            if (!impossible) {
                for (auto p : vertices) {
                    if (u == p || v == p) {
                        continue;
                    }
                    const auto up = graph.getWeight(u, p);
                    const auto vp = graph.getWeight(v, p);
                    if ((up > 0 && vp > 0) || (up < 0 && vp < 0 && up > -INF_WEIGHT / 2 && vp > -INF_WEIGHT / 2)) {
                        relevantVertices.push_back(p);
                        totalSum += std::abs(up - vp);
                    } else if (up > 0 && vp < 0) {
                        deltaU += up;
                        deltaV -= vp;
                        if (vp < -INF_WEIGHT / 2) {
                            impossible = true;
                            break;
                        }
                    } else if  (up < 0 && vp > 0) {
                        deltaV += vp;
                        deltaU -= up;
                        if (up < -INF_WEIGHT / 2) {
                            impossible = true;
                            break;
                        }
                    }
                }
            }
            impossible = impossible || uv <= std::min(deltaU, deltaV);
            if (impossible) {
                continue;
            }
            if (2 * uv > totalSum + deltaU + deltaV) {
                EdgeOperations::merge(graph, u, v);
                onEndOperation();
                return STATUS_GRAPH_MODIFIED;
            }
            bool shouldAttempt = solver.depth <= 8 || uv >= weightThreshold;
            if (!relevantVertices.empty() && shouldAttempt) {
                int sum = calculateSimilarNeighborhoodCost(graph, u, v, relevantVertices, deltaU, deltaV);
                if (sum <= uv) {
                    EdgeOperations::merge(graph, u, v);
                    onEndOperation();
                    return STATUS_GRAPH_MODIFIED;
                }
            }
        }
    }

    onEndOperation();
    return STATUS_NO_CHANGES;
}
int Kernel::calculateSimilarNeighborhoodCost(WeightedGraph &graph,
                                             int u,
                                             int v,
                                             std::list<int>& relevantVertices,
                                             int deltaU,
                                             int deltaV) {
    onStartOperation("calculateSimilarNeighborhood");

    std::vector<int> relevantWeights = std::vector<int>(relevantVertices.size() * 2);
    int xSum = 0;
    int weightIndex = 0;
    for (auto p : relevantVertices) {
        xSum += std::abs(graph.getWeight(u, p));
        relevantWeights.at(weightIndex++) = graph.getWeight(u, p);
        relevantWeights.at(weightIndex++) = graph.getWeight(v, p);
    }
    // TODO transform to class member for performance
    std::vector<int> maximumValues = std::vector<int>(xSum * 2, -INF_WEIGHT);
    maximumValues[0] = 0;
    for (auto i = 0; i < relevantVertices.size(); ++i) {
        const auto fromIndex = (i % 2) * xSum;
        const auto toIndex = ((i + 1) % 2) * xSum;

        const auto x_i = relevantWeights.at(i * 2);
        const auto y_i = relevantWeights.at(i * 2 + 1);

        for (auto x = 0; x < xSum; ++x) {
            auto maxValue = maximumValues.at(fromIndex + x);
            const auto xPlus = x + x_i;
            const auto xMinus = x - x_i;
            if (xPlus >= 0 && xPlus < xSum) {
                maxValue = std::max(maxValue, maximumValues.at(fromIndex + xPlus) - y_i);
            }
            if (xMinus >= 0 && xMinus < xSum) {
                maxValue = std::max(maxValue, maximumValues.at(fromIndex + xMinus) + y_i);
            }
            maximumValues.at(toIndex + x) = maxValue;
        }
    }

    auto finalMaximum = 0;
    const auto finalFromIndex = (relevantVertices.size() % 2) * xSum;
    for (auto x = 0; x < xSum; ++x) {
        finalMaximum = std::max(finalMaximum, std::min(x + deltaU, maximumValues[finalFromIndex + x] + deltaV));
    }

    onEndOperation();
    return finalMaximum;
}