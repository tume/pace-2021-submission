#include <iostream>
#include <list>
#include <algorithm>
#include <cmath>

#include "improved-solver.hpp"
#include "kernel.hpp"
#include "pre-processing.hpp"

#include "../core/graph-components.hpp"
#include "../core/induced-costs.hpp"
#include "../core/conflict-triples.hpp"
#include "../core/edge-operations.hpp"
#include "../util/logging.hpp"
#include "../util/stopwatch.hpp"

std::pair<int, int> ImprovedSolver::findBranchingEdge() {
    onStartOperation("findBranchingEdge");

    const auto& vertices = graph.getVertices();
    auto bestPair = std::make_pair(-1, -1);
    auto bestPairCost = 1;
    auto bestPairMinCost = 0;
    auto bestPairForbidCost = 0;

    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto v = *vit;
            const auto mergeCost = graph.getMergeCost(u, v);
            const auto forbidCost = graph.getForbidCost(u, v);
            const auto weight = graph.getWeight(u, v);

            const auto weightCost = std::max(0, weight);
            const auto minCost = std::min(mergeCost, weightCost);
            const auto sumCost = mergeCost + weightCost;

            if (weight > -INF_WEIGHT / 2 &&
                    (minCost > bestPairMinCost ||
                    (minCost == bestPairMinCost && sumCost > bestPairCost) ||
                    (minCost == bestPairMinCost && sumCost == bestPairMinCost && forbidCost > bestPairForbidCost))) {
                bestPair = std::make_pair(u, v);
                bestPairMinCost = minCost;
                bestPairCost = mergeCost + weightCost;
                bestPairForbidCost = forbidCost;
            }
        }
    }

    onEndOperation();
    return std::move(bestPair);
}

bool ImprovedSolver::recurse() {
    onNodeEntered(graph.getAccumulatedCost());
    ++depth;

    // How many modifications in the graph to start with. Whenever we exit this recurse-function, we want
    // to revert the graph back into the state where it was before applying any of the opreations done here.
    // Since no operation changes the pre-existing modifications, we can do this by reverting applied
    // modifications until we are at the number of pre-existing modifications that the graph had at
    // the start of this function.
    const auto preExistingModifications = graph.getNumModifications();

    bool solvable = Kernel::fullKernel(*this, graph);
    if (!solvable) {
        EdgeOperations::undoBackTo(graph, preExistingModifications);
        --depth;
        onNodeExited();
        return false;
    }


    const auto components = GraphComponents::findConnectedComponents(graph);
    const auto vertices = graph.getVertices();
    // We count the number of "non-trivial" components before deciding on the strategy for recursing.
    // We consider a component trivial if it is automatically a cluster graph, such as a graph with
    // only one or two vertices.
    auto numNonTrivialComponents = 0;

    for (auto &component : components) {
        // TODO detect if component is a clique?
        if (component.size() > 2) {
            numNonTrivialComponents++;
        }
    }
    // If all components are trivial then the whole graph consists of only non-connected clusters,
    // so we can just exit early
    if (numNonTrivialComponents == 0) {
        maxCost -= getRemainingCost() + 1;
        if (!hasSplit) {
            graph.printUnweighted();
            logln("Found complete solution with cost ", graph.getAccumulatedCost());
            logln("Setting max cost to: ", maxCost);
        }

        graph.copyModifications(currentBestSolution);

        EdgeOperations::undoBackTo(graph, preExistingModifications);
        onNodeExited();
        --depth;
        return true;
    }
    // There's some overhead in processing the connected components in a way that the found
    // solution can be found & utilized. We don't want to pay for this overhead unless it
    // is necessary, so we only do it if there is more than 1 non-trivial component
    else if (numNonTrivialComponents > 1) {
        bool solved = true;

        std::list<GraphModification> currentModifications;
        graph.copyModifications(currentModifications);
        auto preBestSolution = std::move(currentBestSolution);
        const auto preMaxCost = maxCost;
        bool hadSplit = hasSplit;
        hasSplit = true;

        for (auto& component : components) {
            // Skip trivial components, there is no need to recurse into something which is already a cluster graph
            if (component.size() <= 2) {
                continue;
            }

            graph.setVertices(component);
            // TODO: do we need to recalculate all induced costs here (and elsewhere where vertices are being set?)
            graph.calculateAllWeightSums();
            solved = branch();
            if (!solved) {
                break;
            }
            // If the solving was successful for the subgraph, then we want to copy the current best solution
            // as the current modification set. This will lower the cost for other components - and if we don't
            // have enough to find a solution for all of them, at least one component's recursion will fail
            graph.setModifications(currentBestSolution);
            // TODO this feels unclear. Would there be a better way?
            maxCost = preMaxCost;
        }

        // If one of the components was not solved successfully, then this search branch can be abandoned.
        // The recursive calls for some components may have found solutions though, and have thus modified
        // the current solution and maximum cost. So before moving forward, we must revert those values
        // into their previous ones, to not affect the search in other branches after this.
        if (!solved) {
            currentBestSolution = std::move(preBestSolution);
            maxCost = preMaxCost;
        } else {
            const auto solutionCost = preMaxCost - getRemainingCost();
            if (!hadSplit) {
                logln("Found complete solution with cost ", solutionCost, ", previous max: ", maxCost);
            }
            maxCost = solutionCost - 1;
        }
        hasSplit = hadSplit;

        graph.setVertices(vertices);
        graph.calculateAllWeightSums();
        graph.setModifications(currentModifications);
        EdgeOperations::undoBackTo(graph, preExistingModifications);

        onNodeExited();
        --depth;
        return solved;
    }
    // Otherwise, there is exactly 1 non-trivial component. Set the graph vertices to that, but otherwise recurse
    // normally
    else {
        for (auto& component : components) {
            if (component.size() > 2) {
                graph.setVertices(component);
                graph.calculateAllWeightSums();
                break;
            }
        }

        bool solved = branch();

        graph.setVertices(vertices);
        graph.calculateAllWeightSums();
        EdgeOperations::undoBackTo(graph, preExistingModifications);
        onNodeExited();
        --depth;
        return solved;
    }
}

bool ImprovedSolver::branch() {
    if (getRemainingCost() < 0) {
        return false;
    }
    auto branchingEdge = findBranchingEdge();

    if (branchingEdge.first == branchingEdge.second) {
        // If we find a solution, then all subsequent solutions must have a smaller cost,
        // as otherwise they are useless to us.
        maxCost -= getRemainingCost() + 1;

        if (!hasSplit) {
            graph.printUnweighted();
            logln("Found complete solution with cost ", graph.getAccumulatedCost());
        }

        graph.copyModifications(currentBestSolution);

        return true;
    }

    const auto preExistingModifications = graph.getNumModifications();

    bool solved = false;
    const auto mergeFirst = graph.getMergeCost(branchingEdge.first, branchingEdge.second) >
        graph.getWeight(branchingEdge.first, branchingEdge.second);

    if (mergeFirst) {
        EdgeOperations::merge(graph, branchingEdge.first, branchingEdge.second);
        solved = recurse();
        EdgeOperations::undoBackTo(graph, preExistingModifications);
    }

    EdgeOperations::forbid(graph, branchingEdge.first, branchingEdge.second);
    solved = recurse() || solved;
    EdgeOperations::undo(graph);

    if (!mergeFirst) {
        EdgeOperations::merge(graph, branchingEdge.first, branchingEdge.second);
        solved = recurse() || solved;
        EdgeOperations::undoBackTo(graph, preExistingModifications);
    }

    return solved;
}

void ImprovedSolver::prepare(int maxCost) {
    this->maxCost = maxCost;
    PreProcessing::performPreProcessing(graph);
}

int ImprovedSolver::getRemainingCost() const {
    return maxCost - graph.getAccumulatedCost();
}

const WeightedGraph& ImprovedSolver::getGraph() const {
    return graph;
}

void ImprovedSolver::printSolution() {
    // Sanity check - most likely the graph should be already undoed to 0, but you never know
    EdgeOperations::undoBackTo(graph, 0);
    auto expandedGraph = WeightedGraph(graph.getVertices().size());

    for (auto& mod : currentBestSolution) {
        if (mod.type == GraphModification::Type::MERGE) {
            EdgeOperations::merge(graph, mod.u, mod.v);
        } else if (mod.type == GraphModification::Type::FORBID) {
            EdgeOperations::forbid(graph, mod.u, mod.v);
        }
    }

    const auto& graphVertices = graph.getVertices();

    // Construct adjacency lists for each vertex according to which vertex they were merged with
    std::vector<std::list<int>> vertexMerges = std::vector<std::list<int>>(expandedGraph.getVertices().size());
    for (auto& mod : currentBestSolution) {
        if (mod.type == GraphModification::Type::MERGE) {
            vertexMerges[mod.u].push_back(mod.v);
            vertexMerges[mod.v].push_back(mod.u);
        }
    }
    // Then augment that adjacency list according to how the current vertex adjacencies are in the solution.
    // If two vertices are adjacent, then they are effectively merged (each remaining edge must be permanent
    // if no more modifications are made, and all permanent medges can always be merged).
    for (auto uit = graphVertices.begin(); uit != graphVertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != graphVertices.end(); ++vit) {
            const auto v = *vit;
            if (graph.getWeight(u, v) > 0) {
                vertexMerges[u].push_back(v);
                vertexMerges[v].push_back(u);
            }
        }
    }

    // Then use Dijkstra's to calculate all clusters/merged-together sets of vertices
    std::list<std::list<int>> mergedVertexGroups;
    for (auto u : expandedGraph.getVertices()) {
        std::list<int> group;
        std::list<int> openVertices;
        openVertices.push_back(u);

        while (!openVertices.empty()) {
            auto v = openVertices.back();
            openVertices.pop_back();
            auto& merges = vertexMerges[v];
            // We clear the list of merge-adjacencies at the end. If the list is empty, then either the vertex
            // was not merged with any other vertex, or it has already been processed by Dijkstra's earlier.
            // In either case, we don't need to process it further.
            if (merges.empty()) {
                continue;
            }

            group.push_back(v);
            for (auto x : merges) {
                openVertices.push_back(x);
            }
            merges.clear();
        }
        if (!group.empty()) {
            mergedVertexGroups.push_back(group);
        }
    }

    for (auto& group : mergedVertexGroups) {
        for (auto u : group) {
            for (auto v : group) {
                if (u != v) {
                    expandedGraph.setWeight(u, v, 1);
                }
            }
        }
    }

    EdgeOperations::undoBackTo(graph, 0);
    const auto vertices = graph.getVertices();

    expandedGraph.printUnweighted();

    int solutionCount = 0;

    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto u = *uit;
            const auto v = *vit;
            // Check if the result graph and original graph have a difference between an edges edgyness
            if (expandedGraph.getWeight(u, v) > 0 && graph.getWeight(u, v) <= 0) {
                std::cout << u + 1 << " " << v + 1 << "\n";
                ++solutionCount;
            } else if (expandedGraph.getWeight(u, v) <= 0 && graph.getWeight(u, v) > 0) {
                std::cout << u + 1 << " " << v + 1 << "\n";
                ++solutionCount;
            }
        }
    }

    logln("Solution has ", solutionCount, " edges");

    printOperationTimes();
}