#pragma once

#include "../core/graph.hpp"

class PreProcessing {
public:
    /**
     * Perform a set of operations to simplify the graph instance before any branching logic
     * has been ran. Note that the solver will first call the normal kernel before branching
     * on an edge, thus there is no need to perform any usual kernelization here. Instead
     * this function may be used for instance reduction for rules that are only applicable at
     * the start of the algorithm.
     */
    static void performPreProcessing(WeightedGraph &graph);

    /**
     * TODO prove to be ok?
     */
    static void forbidNonConflictNonEdges(WeightedGraph &graph);

    /**
     * TODO prove to be ok
     */
    static void mergeNonConflictEdges(WeightedGraph &graph);
};
