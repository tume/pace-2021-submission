#include "pre-processing.hpp"

#include "../core/edge-operations.hpp"
#include "../util/stopwatch.hpp"

void PreProcessing::performPreProcessing(WeightedGraph &graph) {
    onStartOperation("preProcessing");
    PreProcessing::forbidNonConflictNonEdges(graph);
    PreProcessing::mergeNonConflictEdges(graph);
    logln("After preprocessing:");
    graph.printUnweighted();
    onEndOperation();
}

void PreProcessing::forbidNonConflictNonEdges(WeightedGraph &graph) {
    onStartOperation("forbidNonConflictNonEdges");

    const auto& vertices = graph.getVertices();
    auto& usedEdges = graph.getUsedEdges();

    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto v = *vit;
            usedEdges[WeightedGraph::rank(u, v)] = false;
        }
    }
    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto v = *vit;
            const auto uv = graph.getWeight(u, v) > 0;
            for (auto wit = std::next(vit); wit != vertices.end(); ++wit) {
                const auto w = *wit;
                const auto uw = graph.getWeight(u, w) > 0;
                const auto vw = graph.getWeight(v, w) > 0;
                if (uv) {
                    if (uw && !vw) {
                        usedEdges[WeightedGraph::rank(u, v)] = true;
                        usedEdges[WeightedGraph::rank(u, w)] = true;
                        usedEdges[WeightedGraph::rank(v, w)] = true;
                    } else if (vw && !uw) {
                        usedEdges[WeightedGraph::rank(u, v)] = true;
                        usedEdges[WeightedGraph::rank(u, w)] = true;
                        usedEdges[WeightedGraph::rank(v, w)] = true;
                    }
                } else if (uw && vw) {
                    usedEdges[WeightedGraph::rank(u, v)] = true;
                    usedEdges[WeightedGraph::rank(u, w)] = true;
                    usedEdges[WeightedGraph::rank(v, w)] = true;
                }
            }
        }
    }
    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto v = *vit;
            if (graph.getWeight(u, v) <= 0 && !usedEdges[WeightedGraph::rank(u, v)]) {
                EdgeOperations::forbid(graph, u, v);
            }
        }
    }

    onEndOperation();
}

void PreProcessing::mergeNonConflictEdges(WeightedGraph &graph) {
    onStartOperation("mergeNonConflictEdges");

    const auto& vertices = graph.getVertices();
    std::list<std::pair<int, int>> verticesToMerge;
    std::vector<bool> removedVertices = std::vector<bool>(vertices.size(), false);

    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto v = *vit;
            if (graph.getWeight(u, v) > 0 && graph.getMergeCost(u, v) == 0 && !removedVertices[v]) {
                verticesToMerge.emplace_back(u, v);
                removedVertices[v] = true;
            }
        }
    }
    for (auto mergePair : verticesToMerge) {
        EdgeOperations::merge(graph, mergePair.first, mergePair.second);
    }

    onEndOperation();
}

