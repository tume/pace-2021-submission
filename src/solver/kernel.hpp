#pragma once

#include "improved-solver.hpp"

#include "../core/graph.hpp"

class Kernel {
public:
    /**
     * Perform the kernelization procedure on the graph. This may modify the graph and the
     * current cost of the solver. Returns `true` if the resulting instance still seems
     * solvable (according to the kernelization rules), and `false` if the kernelization
     * procedure determines that
     */
    static bool fullKernel(ImprovedSolver& solver, WeightedGraph &graph);

    /**
     * Performs the "light induced cost kernel", which essentially checks if there exists
     * an edge that has to be merged or forbidden based on the induced cost alone (when
     * compared to the remaining cost).
     *
     * The return value is one of the three following:
     * - If the kernel performed an operation, it returns `GRAPH_MODIFIED` to signal that it should
     *   be re-invoked to find the optimal edge
     * - If the instance is determined to be unsolvable with the current remaining cost, the
     *   value `UNSOLVABLE_INSTANCE` is returned.
     * - Otherwise, the kernel returns the single *-edge from the graph with max(icf, icp). This
     *   value is intended to be used in further kernel rules.
     */
    static int quickInducedCostRule(ImprovedSolver& solver, WeightedGraph &graph);

    /**
     * TODO
     */
    static int fullInducedCostRule(ImprovedSolver& solver, WeightedGraph &graph);

    static int lowerBoundRule(ImprovedSolver& solver, WeightedGraph& graph);

    /**
     * Applies the three heavy-edge rules to the graph, attempting to rule out some non-needed
     * modifications based on the unparameterized instance. Returns one of the statuses
     * `STATUS_GRAPH_MODIFIED` or `STATUS_NO_CHANGES` depending on the result of processing.
     */
    static int heavyEdgeRule(WeightedGraph &graph);

    /**
     * TODO
     */
    static int similarNeighborhoodRule(ImprovedSolver& solver, WeightedGraph &graph);

    /**
     * TODO
     */
    static int calculateSimilarNeighborhoodCost(WeightedGraph &graph,
                                                int u,
                                                int v,
                                                std::list<int>& relevantVertices,
                                                int deltaU,
                                                int deltaV);
    /**
     * Applies the "almost clique" rule, which attempts to merge instances that are almost cliques.
     * TODO describe more
     */
    static int almostCliqueRule(ImprovedSolver& solver, WeightedGraph &graph);

    /**
     * TODO proof
     */
    static int nonConflictRule(ImprovedSolver& solver, WeightedGraph &graph);
};

// TODO rename/change the return values in a way that makes sense.
//      perhaps the kernel could make use of its friend access to the solver and use
//      class members there for these?
const auto STATUS_UNSOLVABLE_INSTANCE = -1;
const auto STATUS_GRAPH_MODIFIED = 0;
const auto STATUS_NO_CHANGES = 1;

const auto UNSOLVABLE_INSTANCE = std::make_pair(-1, -1);
const auto GRAPH_MODIFIED = std::make_pair(0, 0);
const auto NO_BEST_EDGE = std::make_pair(1, 1);
