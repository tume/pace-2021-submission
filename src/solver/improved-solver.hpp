#pragma once

#include <tuple>
#include <memory>
#include <utility>
#include <vector>

#include "../core/graph.hpp"

class Kernel;

class ImprovedSolver {
private:
    WeightedGraph graph;
    int maxCost;
    int depth;
    bool hasSplit;

    std::list<GraphModification> currentBestSolution;
    /**
     * This variable is used by the kernelization to communicate between various parts of
     * the kernel.
     */
    std::pair<int, int> kernelBestEdge;

    friend class Kernel;
public:
    explicit ImprovedSolver(WeightedGraph graph): graph(std::move(graph)),
                                                  maxCost(0),
                                                  depth(0),
                                                  hasSplit(false),
                                                  currentBestSolution(0) {};
    /**
     * Prepares the solver and the input graph for a run with the given maximum cost.
     * This method should be invoked before attempting to invoke any other methods of
     * this class.
     */
    void prepare(int maxCost);

    /**
     * TODO
     */
    bool recurse();

    /**
     * TODO
     */
    bool branch();

    /**
     * TODO
     */
    std::pair<int, int> findBranchingEdge();

    /**
     * Returns a reference to the graph instance owned by the solver.
     */
    const WeightedGraph& getGraph() const;

    /**
     * Returns maxCost - currentCost, i.e. the remaining cost/depth/budget that the solver
     * may still use.
     */
    int getRemainingCost() const;

    /**
     * Prints the solution as a list of unweighted edge modifications that need to be done
     * to the original graph in order to create a cluster graph.
     *
     * Note: this modifies the graph instance! Do not call during the execution of the search.
     */
    void printSolution();
};

