#pragma once

#include <string>
#include <list>
#include <map>
#include <chrono>

typedef std::chrono::duration<double, std::milli> duration ;
typedef std::chrono::time_point<std::chrono::high_resolution_clock> time_point ;

class StopWatch {
public:
    void onEnterNode(int depth);
    void onExitNode();

    void logStart(std::string operation);
    void logEnd();

    void printTimes();

    void reset();

    static StopWatch& getInstance() {
        static StopWatch stopWatch;
        return stopWatch;
    }
private:
    int nodeCounter;
    std::list<int> depthStack;
    std::list<std::pair<std::string, time_point>> operationStack;
    time_point startTime;
    time_point lastProgressTime;

    std::map<std::string, duration> operationTimes;
    std::map<std::string, duration> operationTimesWithContext;
    std::map<int, duration> timesByLevel;
};

#ifdef BENCHMARK
#define onNodeEntered(depth) StopWatch::getInstance().onEnterNode(depth)
#define onNodeExited(depth) StopWatch::getInstance().onExitNode(depth)
#define onStartOperation(name) StopWatch::getInstance().logStart(name)
#define onEndOperation() StopWatch::getInstance().logEnd()
#define printOperationTimes() StopWatch::getInstance().printTimes()
#define resetStopwatch() StopWatch::getInstance().reset()
#endif
#ifndef BENCHMARK
#define onNodeEntered(depth) {}
#define onNodeExited(depth) {}
#define onStartOperation(name) {}
#define onEndOperation() {}
#define printOperationTimes() {}
#define resetStopwatch() {}
#endif
