#include <algorithm>
#include <iostream>
#include <iomanip>
#include <exception>

#include "stopwatch.hpp"

void StopWatch::onEnterNode(int depth) {
    ++nodeCounter;
    auto currentTime = std::chrono::high_resolution_clock::now();
    duration durationMillis = currentTime - lastProgressTime;
    if (durationMillis.count() > 60000) {
        std::cout << "Current search nodes: " << nodeCounter << ", at depth: " << depth << " upon logging\n";
        lastProgressTime = currentTime;
    }
}

void StopWatch::onExitNode() {
}

void StopWatch::logStart(std::string operation) {
    operationStack.emplace_back(operation, std::chrono::high_resolution_clock::now());
}

void StopWatch::logEnd() {
    auto lastOperation = operationStack.back();
    operationStack.pop_back();
    auto duration = std::chrono::high_resolution_clock::now() - lastOperation.second;

    if (operationTimes.find(lastOperation.first) == operationTimes.end()) {
        operationTimes[lastOperation.first] = duration;
    } else {
        operationTimes[lastOperation.first] += duration;
    }

    std::string contextualKey = "";
    for (auto& s : operationStack) {
        contextualKey += s.first;
        contextualKey += " > ";
    }
    contextualKey += lastOperation.first;
    if (operationTimesWithContext.find(contextualKey) == operationTimesWithContext.end()) {
        operationTimesWithContext[contextualKey] = duration;
    } else {
        operationTimesWithContext[contextualKey] += duration;
    }
}

void StopWatch::reset() {
    nodeCounter = 0;
    depthStack.clear();
    operationStack.clear();
    operationTimes.clear();
    operationTimesWithContext.clear();
    timesByLevel.clear();
    startTime = std::chrono::high_resolution_clock::now();
    lastProgressTime = std::chrono::high_resolution_clock::now();
}

std::vector<std::pair<std::string, double>> getTimeSortedList(std::map<std::string, duration> &durations) {
    std::vector<std::pair<std::string, double>> list;

    for (auto& entry : durations) {
        list.emplace_back(entry.first,  entry.second.count());
    }
    std::sort(list.begin(), list.end(), [](auto& left, auto& right) {
        return left.second > right.second;
    });

    return std::move(list);
}

void StopWatch::printTimes() {
    auto operationTimes = getTimeSortedList(this->operationTimes);
    auto contextualOperationTimes = getTimeSortedList(this->operationTimesWithContext);
    auto endTime = std::chrono::high_resolution_clock::now();
    auto totalDuration = endTime - this->startTime;

    std::cout << "Search tree nodes: " << nodeCounter << "\n";

    std::cout << "\nOperation times, suboperations included:\n";
    for (auto& pair : operationTimes) {
        std::cout << pair.first << ": " << pair.second << "ms\n";
    }

    std::cout << "\nContextual operation times:\n";
    for (auto& pair : contextualOperationTimes) {
        std::cout << pair.first << ": " << pair.second << "ms\n";
    }

    std::cout << "\nTotal time: " << std::chrono::duration_cast<std::chrono::milliseconds>(totalDuration).count() << "ms\n";
}