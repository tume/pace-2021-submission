#pragma once

#include <iostream>
#include <utility>

inline void log() {}

template<typename First, typename ...Rest>
inline void log(First && first, Rest && ...rest) {
    #ifdef DEBUG
    std::cout << std::forward<First>(first);
    log(std::forward<Rest>(rest)...);
    #endif
}

inline void logln() {
    #ifdef DEBUG
    std::cout << std::endl;
    #endif
}

template<typename First, typename ...Rest>
inline void logln(First && first, Rest && ...rest) {
    #ifdef DEBUG
    std::cout << std::forward<First>(first);
    logln(std::forward<Rest>(rest)...);
    #endif
}
