#include <cmath>

#include "min-cut.hpp"
#include "edge-operations.hpp"

#include "../util/stopwatch.hpp"

int MinCut::calculateMinimumCut(WeightedGraph &graph, const std::list<int> &vertices) {
    onStartOperation("minCut");

    auto minCut = INF_WEIGHT;
    const auto originalVertices = graph.getVertices();

    graph.setVertices(vertices);
    for (auto uit = graph._vertices.begin(); uit != graph._vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != graph._vertices.end(); ++vit) {
            const auto v = *vit;
            const auto rank = WeightedGraph::rank(u, v);
            graph._secondaryEdgeWeights[rank] = graph._edgeWeights[rank];
        }
    }

    while (graph.getVertices().size() > 1) {
        const auto someCut = calculateSomeMinSTCut(graph);
        minCut = std::min(minCut, someCut);
    }

    graph.setVertices(originalVertices);
    // Restore edge weights for all modified edges
    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            const auto v = *vit;
            const auto rank = WeightedGraph::rank(u, v);
            graph._edgeWeights[rank] = graph._secondaryEdgeWeights[rank];
        }
    }

    onEndOperation();

    return minCut;
}

int MinCut::calculateSomeMinSTCut(WeightedGraph &graph) {
    // Mark all vertices as unused = not yet selected
    for (auto v : graph._vertices) {
        graph._markedVertices[v] = false;
    }

    int s, t = -1;

    // Select vertices one by one to be included in the vertex set
    for (auto iteration = 0; iteration < graph._vertices.size(); ++iteration) {
        int bestConnectivity = -1;
        int bestVertex = -1;

        // Iterate through all vertices to find the one with best connectivity
        for (auto u : graph._vertices) {
            // Skip selecting vertices already in the set
            if (graph._markedVertices[u]) {
                continue;
            }
            int connectivity = 0;

            // Calculate the connectivity of the vertex
            for (auto v : graph._vertices) {
                if (!graph._markedVertices[v] || u == v) {
                    continue;
                }
                const auto weight = graph.getWeight(u, v);
                connectivity += std::max(0, weight);
            }
            // If u is more connected to the set of currently selected vertices than
            // any other vertex, then we select u to be added to the vertex set
            if (connectivity > bestConnectivity) {
                bestConnectivity = connectivity;
                bestVertex = u;
            }
        }

        graph._markedVertices[bestVertex] = true;
        if (iteration == graph._vertices.size() - 2) {
            s = bestVertex;
        } else if (iteration == graph._vertices.size() - 1) {
            t = bestVertex;
        }
    }

    int tEdgeWeightSum = 0;
    for (auto u : graph._vertices) {
        if (u == t) {
            continue;
        }
        tEdgeWeightSum += std::max(0, graph.getWeight(t, u));
    }

    MinCut::merge(graph, s, t);

    return tEdgeWeightSum;
}

void MinCut::merge(WeightedGraph& graph, int u, int v) {
    onStartOperation("minCutMerge");

    for (auto it = graph._vertices.begin(); it != graph._vertices.end(); ++it) {
        auto p = *it;
        if (p == u) {
            continue;
        } else if (p == v) {
            it = graph._vertices.erase(it);
            it--;
            continue;
        }
        const auto up = graph.getWeight(u, p);
        const auto vp = graph.getWeight(v, p);

        // Use w to denote the merged vertex u & v
        const auto wp = std::max(0, up) + std::max(0, vp);
        graph.setWeight(u, p, wp);
    }

    onEndOperation();
}
