#pragma once

#include "graph.hpp"

class InducedCosts {
public:
    /**
     * Calculates all induced forbid and merge costs for all vertex pairs of the graph,
     * caching the results in the corresponding vectors.
     */
    static void calculateAllInducedCosts(WeightedGraph &graph);

    /**
     * Calculates the minimum cost (=sum weight of edge modifications) that is required when
     * forbidding the edge {u,v}.
     */
    static int calculateForbidCost(const WeightedGraph &graph, int u, int v);

    /**
     * Calculates the minimum cost (=sum weight of edge modifications) that is required when
     * merging the vertices u and v.
     */
    static int calculateMergeCost(const WeightedGraph &graph, int u, int v);
};