#pragma once

#include "graph.hpp"

const int STRONG_CLIQUE = 1;
const int WEAK_CLIQUE = 0;
const int NOT_CLIQUE = -1;

class GraphComponents {
public:
    /**
     * Returns a list of vertex-lists, representing the connected components of the
     * graph. Vertices u and v are considered connected if weight(u, v) > 0, i.e. a
     * negative or zero weight is considered a non-edge.
     */
    static std::list<std::list<int>> findConnectedComponents(WeightedGraph& graph);

    /**
     * Returns an integer signifying if the given list of vertices forms a clique of the size.
     * If there is an edge with positive weight between all the given vertices, this
     * function returns `STRONG_CLIQUE` to signal that the vertices form a strong clique.
     * Otherwise, if there are no negative weight edges between any of the vertices,
     * this function returns `WEAK_CLIQUE` to signal that the vertices form a weak clique.
     * Otherwise, this function returns `NOT_CLIQUE` to signal that the vertices do not form
     * a clique of the right size.
     */
    static int findCliqueType(WeightedGraph& graph, const std::list<int> &vertices);
};