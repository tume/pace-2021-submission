#pragma once

#include <string>
#include <vector>
#include <list>
#include <exception>
#include <iostream>
#include <string>

#include "../util/logging.hpp"


/**
 * Represents a modification that has been done to a WeightedGraph-instance.
 * In addition to containing sufficient information for reversing the modification,
 * each entry also contains the total accumulated cost `k` for a Cluster Editing
 * algorithm. When GraphModification-structs are stored in a list, this value can
 * be used to keep track of the total running cost when all entries of the list
 * are applied in order.
 */
struct GraphModification {
    enum Type {
        FORBID,
        MERGE
    };
    /**
     * The type of the modification: was an edge between two vertices merged, or
     * was it forbidden.
     */
    Type type;
    /**
     * One of the vertices affected by this modification.
     */
    int u;
    /**
     * The other vertex affected by this modification.
     */
    int v;
    /**
     * The original weight of the edge {u,v}. If `type` is `MERGE`, then the
     * value of this is undefined.
     */
    int weight;
    /**
     * The total accumulated cost of all operations up to (and including) this one.
     * Note that this is not a value of this modification alone, but instead a
     * compound value of all previous modifications. The value has been added to
     * this struct for convenience.
     */
    int accumulatedCost;
};

/**
 * The actual "implementation" of the ConflictTriple struct belongs to the ConflictTriples
 * file, as it is the one providing the tools to interact with it. However, the
 * `WeightedGraph` must maintain a reference into a vector of these, so we have to
 * declare the struct here.
 */
struct ConflictTriple {
    short u;
    short v;
    short w;
    short resolveCost;

    ConflictTriple(short x, short y, short z, short resolveCost) {
        if (x < y && x < z) {
            u = x;
            if (y < z) {
                v = y;
                w = z;
            } else {
                v = z;
                w = y;
            }
        } else if (y < z) {
            u = y;
            if (x < z) {
                v = x;
                w = z;
            } else {
                v = z;
                w = x;
            }
        } else {
            u = z;
            if (x < y) {
                v = x;
                w = y;
            } else {
                v = y;
                w = x;
            }
        }
        this->resolveCost = resolveCost;
    }

    bool operator<(const ConflictTriple& other) const {
        if (u < other.u) {
            return true;
        } else if (other.u < u) {
            return false;
        }
        if (v < other.v) {
            return true;
        } else if (other.v < v) {
            return false;
        }
        return w < other.w;
    }

    bool operator==(const ConflictTriple& other) const {
        return u == other.u && v == other.v && w == other.w;
    }

    bool operator!=(const ConflictTriple& other) const {
        return u != other.u || v != other.v || w != other.w;
    }

    std::string toString() const {
        return "{" + std::to_string(u) + "," + std::to_string(v) + "," + std::to_string(w) + "}";
    }
};
const ConflictTriple NO_CONFLICT_TRIPLE { -1, -1, -1, 0 };

// Forward declare the friend classes where algorithmic logic has been separated
class GraphComponents;
class InducedCosts;
class ConflictTriples;
class EdgeOperations;
class MinCut;

/**
 * TODO
 */
class WeightedGraph {
private:
    std::vector<int> _edgeWeights;
    /**
     * TODO
     */
    std::vector<int> _secondaryEdgeWeights;
    std::vector<int> _forbidCosts;
    std::vector<int> _mergeCosts;
    /**
     * How many vertices does each vertex represent: when the edge {u,v} is merged, we increase
     * the count of u by the count of v, as u now represent both the vertices that u and v
     * previously represented.
     */
    std::vector<int> _vertexCounts;
    std::vector<int> _conflictCounts;

    /**
     * For a vertex u, contains the sum of all positive edge weights {u,v} for every vertex v != u.
     * Essentially this contains the weight sum for all edges of a vertex.
     */
    std::vector<int> _vertexNeighborSums;

    /**
     * For a vertex u, contains the sum of the absolute values of all weights {u,v} for every vertex v != u.
     * Essentially this contains the weight sum for all edges and non-edges of a vertex, so that the weight
     * of a non-edge is counted as a positive integer.
     */
    std::vector<int> _vertexAllEdgeSums;

    std::list<int> _vertices;
    std::list<GraphModification> _modificationHistory;
    int _maxVertexIndex;
    int _totalDisjointConflictLowerBound;
    int _failureDetectedOnIndex;

    /**
     * This vector is used by some graph operations internally.
     * TODO use a bitmap
     */
    std::vector<bool> _markedEdges;

    /**
     * This list is used by some graph operations internally.
     */
    std::list<std::pair<int, int>> _openEdges;

    /**
     * This value is used by some graph operations internally.
     * TODO use a bitmap
     */
    std::vector<bool> _markedVertices;

    std::vector<ConflictTriple> _conflictTriples;

    /**
     * TODO explain well: we keep track of how much resolving a certain edge would affect
     *      the conflict lower bound.
     */
    std::vector<int> _edgeLowerBoundChange;

    /**
     * TODO bitmap
     */
    std::vector<bool> _conflictFreeEdges;

    std::list<std::pair<int, int>> _checkConflictEdges;

    friend class GraphComponents;
    friend class InducedCosts;
    friend class ConflictTriples;
    friend class EdgeOperations;
    friend class MinCut;
public:
    explicit WeightedGraph(int numVertices): _vertices(0),
                                             _markedVertices(numVertices, false),
                                             _openEdges(0),
                                             _checkConflictEdges(0),
                                             _failureDetectedOnIndex(-1),
                                             _maxVertexIndex(numVertices),
                                             _vertexCounts(numVertices, 1),
                                             _edgeWeights(numVertices * (numVertices - 1) / 2, -1),
                                             _edgeLowerBoundChange(numVertices * (numVertices - 1) / 2, 0),
                                             _secondaryEdgeWeights(numVertices * (numVertices - 1) / 2, -1),
                                             _forbidCosts(numVertices * (numVertices - 1) / 2, 0),
                                             _mergeCosts(numVertices * (numVertices - 1) / 2, 0),
                                             _conflictFreeEdges(numVertices * (numVertices - 1) / 2, false),
                                             _conflictTriples(numVertices * (numVertices - 1) / 2, NO_CONFLICT_TRIPLE),
                                             _vertexNeighborSums(numVertices, 0),
                                             _vertexAllEdgeSums(numVertices, 0),
                                             _markedEdges(numVertices * (numVertices - 1) / 2, false),
                                             _conflictCounts(numVertices * (numVertices - 1) / 2, 0),
                                             _totalDisjointConflictLowerBound(0),
                                             _modificationHistory(0) {
        for (auto i = 0; i < numVertices; ++i) {
            _vertices.push_back(i);
        }
    }

    /**
     * Calculates initial values for `_vertexNeighborSums` and `_vertexAllEdgeSums`.
     */
    void calculateAllWeightSums();

    /**
     * Returns the internal weight vector of the graph. Note that this may contain rows/columns
     * for vertices not currently present in the graph.
     */
    std::vector<int> getAllWeights() const;

    /**
     * Updates all weights of the graph to match the ones in the given vector. The length of the
     * vector must be the exact same as the one used by the graph - it is recommended that this
     * function is only used with vectors received from the `getAllWeights`-function.
     */
    void setAllWeights(const std::vector<int>& weights);

    /**
     * Updates the current vertices of the graph to match the ones in the given list.
     */
    void setVertices(const std::list<int>& vertices);

    /**
     * Copies the current modification history of the graph into the given list.
     */
    void copyModifications(std::list<GraphModification> &into) const;

    /**
     * Sets the current modification history of the graph according to the given list.
     * Note that this does not perform any operations to actually modify the graph, thus
     * using this method may result in a graph where the modification history does not
     * correspond to the state of graph. Use carefully.
     */
    void setModifications(std::list<GraphModification> &from);

    /**
     * Returns the rank/index for edge {u,v} for accessing a value stored for it
     * in a lower-diagonal triangle matrix.
     */
    inline static int rank(int u, int v) {
        if (u < v) {
            const auto t = u;
            u = v;
            v = t;
        }
#ifdef DEBUG
        if (u == v) {
            logln("Attempting to call rank(", u, ", ", v, ")");
            throw std::invalid_argument("Cannot pass the same vertex twice to edgeRank");
        }
#endif
        return ((u * u - u) >> 1) + v;
    }

    /**
     * Gets the previously stored & calculated merge cost for the edge {u,v}.
     * Returns 0 if the value has not been calculated and stored previously.
     */
    inline int getMergeCost(int u, int v) const {
#ifdef DEBUG
        return _mergeCosts.at(rank(u, v));
#endif
#ifndef DEBUG
        return _mergeCosts[rank(u, v)];
#endif
    }

    /**
     * Gets the previously stored & calculated forbid cost for the edge {u,v}.
     * Returns 0 if the value has not been calculated and stored previously.
     */
    inline int getForbidCost(int u, int v) const {
#ifdef DEBUG
        return _forbidCosts.at(rank(u, v));
#endif
#ifndef DEBUG
        return _forbidCosts[rank(u, v)];
#endif
    }

    /**
     * Returns the edge weight between the two vertices u and v.
     */
    inline int getWeight(int u, int v) const {
#ifdef DEBUG
        return _edgeWeights.at(rank(u, v));
#endif
#ifndef DEBUG
        return _edgeWeights[rank(u, v)];
#endif
    }

    inline int getEdgeLowerBoundChange(int u, int v) const {
        return _edgeLowerBoundChange[rank(u, v)];
    }

    /**
     * Updates the weight of the edge between vertices u and v to be the given integer.
     */
    inline void setWeight(int u, int v, int weight) {
        _edgeWeights[rank(u, v)] = weight;
    }

    /**
     * Returns the vertex-list of the graph. Vertices may be removed if the `merge`-function
     * is invoked.
     */
    inline const std::list<int>& getVertices() const {
        return _vertices;
    }

    /**
     * TODO
     */
    inline std::vector<bool>& getUsedEdges() {
        return _markedEdges;
    }

    /**
     * Returns the total accumulated cost of all applied graph modifications.
     * This is a constant time operation.
     */
    inline int getAccumulatedCost() const {
        if (_modificationHistory.empty()) {
            return 0;
        }
        return _modificationHistory.back().accumulatedCost;
    }

    inline std::list<std::pair<int, int>>& getPotentialConflictFreeEdges() {
        return _checkConflictEdges;
    }

    /**
     * Returns the number of modifications that have been applied to the graph.
     */
    inline int getNumModifications() const {
        return _modificationHistory.size();
    }

    /**
     * Returns the amount of conflict triples that the edge {u,v} is part of. Note that
     * this value is not calculated with this method: instead `calculateAllConflictCounts`
     * should be called prior to this to calculate the value.
     */
    inline int getConflictCount(int u, int v) const {
        return _conflictCounts[rank(u, v)];
    }

    /**
     * TODO
     */
    inline int getVertexNeighborSum(int u) const {
        return _vertexNeighborSums[u];
    }

    inline int getFailureDetectedOnIndex() const {
        return _failureDetectedOnIndex;
    }

    /**
     * TODO
     */
    inline int getVertexAllEdgeSum(int u) const {
        return _vertexAllEdgeSums[u];
    }

    /**
     * Returns the largest vertex number, i.e. the maximum possible index required by any
     * vertex when accessing a vector for them.
     */
    inline int getMaxVertex() const {
        return _maxVertexIndex;
    }

    inline int getTotalDisjointConflictLowerBound() const {
        return _totalDisjointConflictLowerBound;
    }

    /**
     * TODO
     */
     inline int getMergeCount(int vertex) const {
         return _vertexCounts[vertex];
     }

     /**
      * TODO
      */
     inline bool isMarked(int u, int v) const {
         return _markedEdges[WeightedGraph::rank(u, v)];
     }

     inline bool isConflictFree(int u, int v) const {
         return _conflictFreeEdges[WeightedGraph::rank(u, v)];
     }

    /**
     * Creates a new instance of this class from the given input string.
     * The string is expected to be in the PACE 2021 input instance format.
     */
    static WeightedGraph fromString(std::string definition);

    /**
     * Creates a new instance of this class from the standard input stream.
     * The input is expected to be in the PACE 2021 input instance format.
     */
    static WeightedGraph fromStdin();

    /**
     * Prints the graph, showing non-edges, zero-edges and edges but not their exact weights.
     * No output is shown unless the DEBUG-flag has been set.
     */
    void printUnweighted() const;

    /**
     * TODO
     */
    void printMarkedEdges() const;

    /**
     * Prints all modifications that have been applied to the graph but not undoed.
     * Each modification is printed on its own line.
     * No output is shown unless the DEBUG-flag has been set.
     */
    void printModifications() const;
};

/**
 * This weight is used to forbid edges, and it should be a small enough value that no realistic
 * instance will ever end up even near it with real edge weights during the execution of some
 * algorithm. However, the cost cannot be the minimum value for 32bit signed integers, because
 * merging of vertices may create edge weights that are multiples of this value.
 */
const int INF_WEIGHT = 100000;
