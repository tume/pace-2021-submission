#include <cmath>

#include "edge-operations.hpp"
#include "induced-costs.hpp"
#include "conflict-triples.hpp"

#include "../util/stopwatch.hpp"

void EdgeOperations::merge(WeightedGraph& graph, int u, int v) {
    onStartOperation("merge");
    if (v < u) {
        auto tmp = u;
        u = v;
        v = tmp;
    }

    auto mergeCost = graph.getMergeCost(u, v);
    // Its alright to leave the weight unset - all the needed information for undoing will be kept
    // in the vertex-weight matrix.
    graph._modificationHistory.push_back({
        GraphModification::Type::MERGE,
        u,
        v,
        0,
        graph.getAccumulatedCost() + mergeCost
    });
    graph._vertexCounts[u] += graph._vertexCounts[v];

    for (auto it = graph._vertices.begin(); it != graph._vertices.end(); ++it) {
        auto p = *it;
        if (p == u) {
            continue;
        } else if (p == v) {
            it = graph._vertices.erase(it);
            it--;
            continue;
        }
        const auto up = graph.getWeight(u, p);
        const auto vp = graph.getWeight(v, p);

        // Use w to denote the merged vertex u & v
        const auto wp = up + vp;
        graph.setWeight(u, p, wp);

        // Update neighborhood weight sums for all relevant vertices
        if (up > 0) {
            graph._vertexNeighborSums[u] -= up;
            graph._vertexNeighborSums[p] -= up;
            graph._vertexAllEdgeSums[u] -= up;
            graph._vertexAllEdgeSums[p] -= up;
        } else {
            graph._vertexAllEdgeSums[u] += up;
            graph._vertexAllEdgeSums[p] += up;
        }
        if (vp > 0) {
            graph._vertexNeighborSums[p] -= vp;
            graph._vertexAllEdgeSums[p] -= vp;
        } else {
            graph._vertexAllEdgeSums[p] += vp;
        }
        if (wp > 0) {
            graph._vertexNeighborSums[u] += wp;
            graph._vertexNeighborSums[p] += wp;
            graph._vertexAllEdgeSums[u] += wp;
            graph._vertexAllEdgeSums[p] += wp;
        } else {
            graph._vertexAllEdgeSums[u] -= wp;
            graph._vertexAllEdgeSums[p] -= wp;
        }

        // Calculate new icp and icf for every {p,x}
        for (auto xit = std::next(it); xit != graph._vertices.end(); ++xit) {
            const auto x = *xit;
            if (x == u || x == v) {
                continue;
            }
            const auto ux = graph.getWeight(u, x);
            const auto vx = graph.getWeight(v, x);

            auto icp = graph.getMergeCost(p, x);
            auto icf = graph.getForbidCost(p, x);

            if ((ux > 0 && up <= 0) || (ux <= 0 && up > 0)) {
                icp -= std::min(std::abs(ux), std::abs(up));
            } else if (ux > 0 && up > 0) {
                icf -= std::min(ux, up);
            }
            if ((vx > 0 && vp <= 0) || (vx <= 0 && vp > 0)) {
                icp -= std::min(std::abs(vx), std::abs(vp));
            } else if (vx > 0 && vp > 0) {
                icf -= std::min(vx, vp);
            }

            const auto wx = ux + vx;
            if ((wp > 0 && wx <= 0) || (wp <= 0 && wx > 0)) {
                icp += std::min(std::abs(wp), std::abs(wx));
            } else if (wp > 0 && wx > 0) {
                icf += std::min(wx, wp);
            }
            graph._mergeCosts[WeightedGraph::rank(p, x)] = icp;
            graph._forbidCosts[WeightedGraph::rank(p, x)] = icf;
        }
    }

    // Calculate new merge/forbid costs for each {u,p} for every vertex p.
    for (auto p : graph._vertices) {
        if (p == u) {
            continue;
        }
        graph._mergeCosts[WeightedGraph::rank(u, p)] = InducedCosts::calculateMergeCost(graph, u, p);
        graph._forbidCosts[WeightedGraph::rank(u, p)] = InducedCosts::calculateForbidCost(graph, u, p);
    }
    // Handle {u,v} being an edge and how it affects neighbor costs
    const auto uv = graph.getWeight(u, v);
    if (uv > 0) {
        graph._vertexNeighborSums[u] -= uv;
        graph._vertexAllEdgeSums[u] -= uv;
    } else {
        graph._vertexAllEdgeSums[u] += uv;
    }

    ConflictTriples::updateConflictTriplesAfterMerge(graph, u, v);

    onEndOperation();
}

void EdgeOperations::forbid(WeightedGraph& graph, int u, int v) {
    onStartOperation("forbid");

    const auto weight = graph.getWeight(u, v);
    // Forbidding non-edges should cost nothing.
    const auto cost = std::max(0, weight);
    graph._modificationHistory.push_back({GraphModification::Type::FORBID, u, v, weight, graph.getAccumulatedCost() + cost});

    graph.setWeight(u, v, graph.getWeight(u, v) - INF_WEIGHT);

    // After becoming forbidden, the edge has an induced merge cost of infinity
    graph._mergeCosts[WeightedGraph::rank(u, v)] = graph.getMergeCost(u, v) + INF_WEIGHT - std::max(weight, 0);
    if (weight > 0) {
        graph._vertexNeighborSums[u] -= weight;
        graph._vertexNeighborSums[v] -= weight;
        graph._vertexAllEdgeSums[u] += INF_WEIGHT - weight - weight;
        graph._vertexAllEdgeSums[v] += INF_WEIGHT - weight - weight;
        graph._forbidCosts[WeightedGraph::rank(u, v)] = graph._forbidCosts[WeightedGraph::rank(u, v)] - weight;
    } else {
        graph._vertexAllEdgeSums[u] += INF_WEIGHT;
        graph._vertexAllEdgeSums[v] += INF_WEIGHT;
    }

    // Recalculate induced costs for all relevant edges
    for (auto x : graph._vertices) {
        if (x == u || x == v) {
            continue;
        }
        const auto ux = graph.getWeight(u, x);
        const auto vx = graph.getWeight(v, x);

        if (weight > 0) {
            if (ux > 0) {
                graph._forbidCosts[WeightedGraph::rank(v, x)] = graph.getForbidCost(v, x) - std::min(ux, weight);
                graph._mergeCosts[WeightedGraph::rank(v, x)] = graph.getMergeCost(v, x) + ux;
            } else {
                graph._mergeCosts[WeightedGraph::rank(v, x)] = graph.getMergeCost(v, x) - std::min(-ux, weight);
            }
            if (vx > 0) {
                graph._forbidCosts[WeightedGraph::rank(u, x)] = graph.getForbidCost(u, x) - std::min(vx, weight);
                graph._mergeCosts[WeightedGraph::rank(u, x)] = graph.getMergeCost(u, x) + vx;
            } else {
                graph._mergeCosts[WeightedGraph::rank(u, x)] = graph.getMergeCost(u, x) - std::min(-vx, weight);
            }
        } else {
            if (ux > 0) {
                graph._mergeCosts[WeightedGraph::rank(v, x)] = graph.getMergeCost(v, x) - std::min(ux, -weight) + ux;
            }
            if (vx > 0) {
                graph._mergeCosts[WeightedGraph::rank(u, x)] = graph.getMergeCost(u, x) - std::min(vx, -weight) + vx;
            }
        }
    }

    ConflictTriples::updateConflictTriplesAfterForbid(graph, u, v);

    onEndOperation();
}

void EdgeOperations::undoBackTo(WeightedGraph& graph, int targetNumModifications) {
    const auto currentModifications = graph.getNumModifications();
    for (auto i = 0; i < currentModifications - targetNumModifications; ++i) {
        undo(graph);
    }
}

void EdgeOperations::undo(WeightedGraph& graph) {
    onStartOperation("undo");

    const auto lastModification = graph._modificationHistory.back();
    // Remove the last modification from history
    graph._modificationHistory.pop_back();

    if (lastModification.type == GraphModification::Type::MERGE) {
        onStartOperation("unmerge");
        const auto u = lastModification.u;
        const auto v = lastModification.v;

        auto vPosition = graph._vertices.end();
        for (auto it = graph._vertices.begin(); it != graph._vertices.end(); ++it) {
            auto p = *it;
            // v should be added into the position where it is between a smaller and a larger
            // vertex. This should guarantee placement back into its original location
            if (vPosition == graph._vertices.end() && p > v) {
                vPosition = it;
            }
            // Skip self edges
            if (p == u) {
                continue;
            }
            const auto wp = graph.getWeight(u, p);
            const auto vp = graph.getWeight(v, p);
            const auto up = wp - vp;

            // Update neighborhood weight sums for all relevant vertices
            if (up > 0) {
                graph._vertexNeighborSums[u] += up;
                graph._vertexNeighborSums[p] += up;
                graph._vertexAllEdgeSums[u] += up;
                graph._vertexAllEdgeSums[p] += up;
            } else {
                graph._vertexAllEdgeSums[u] -= up;
                graph._vertexAllEdgeSums[p] -= up;
            }
            if (vp > 0) {
                graph._vertexNeighborSums[p] += vp;
                graph._vertexAllEdgeSums[p] += vp;
            } else {
                graph._vertexAllEdgeSums[p] -= vp;
            }
            if (wp > 0) {
                graph._vertexNeighborSums[u] -= wp;
                graph._vertexNeighborSums[p] -= wp;
                graph._vertexAllEdgeSums[u] -= wp;
                graph._vertexAllEdgeSums[p] -= wp;
            } else {
                graph._vertexAllEdgeSums[u] += wp;
                graph._vertexAllEdgeSums[p] += wp;
            }

            // A reverse update that was done on merge - subtract the weight of {v,p} from {u,p}
            graph.setWeight(u, p, up);

            // Calculate new icp and icf for every {p,x}
            for (auto xit = graph._vertices.begin(); xit != it; ++xit) {
                const auto x = *xit;
                if (x == u || x == v) {
                    continue;
                }
                const auto ux = graph.getWeight(u, x);
                const auto vx = graph.getWeight(v, x);

                auto icp = graph.getMergeCost(p, x);
                auto icf = graph.getForbidCost(p, x);

                if ((ux > 0 && up <= 0) || (ux <= 0 && up > 0)) {
                    icp += std::min(std::abs(ux), std::abs(up));
                } else if (ux > 0 && up > 0) {
                    icf += std::min(ux, up);
                }
                if ((vx > 0 && vp <= 0) || (vx <= 0 && vp > 0)) {
                    icp += std::min(std::abs(vx), std::abs(vp));
                } else if (vx > 0 && vp > 0) {
                    icf += std::min(vx, vp);
                }

                const auto wx = ux + vx;
                if ((wp > 0 && wx <= 0) || (wp <= 0 && wx > 0)) {
                    icp -= std::min(std::abs(wp), std::abs(wx));
                } else if (wp > 0 && wx > 0) {
                    icf -= std::min(wx, wp);
                }
                graph._mergeCosts[WeightedGraph::rank(p, x)] = icp;
                graph._forbidCosts[WeightedGraph::rank(p, x)] = icf;
            }
        }
        // Add the vertex v back to its rightful place
        graph._vertices.insert(vPosition, v);
        graph._vertexCounts[u] -= graph._vertexCounts[v];

        // Calculate new merge/forbid costs for each {u,p} for every vertex p.
        for (auto p : graph._vertices) {
            if (p == u) {
                continue;
            }
            graph._mergeCosts[WeightedGraph::rank(u, p)] = InducedCosts::calculateMergeCost(graph, u, p);
            graph._forbidCosts[WeightedGraph::rank(u, p)] = InducedCosts::calculateForbidCost(graph, u, p);
        }
        const auto uv = graph.getWeight(u, v);
        if (uv > 0) {
            graph._vertexNeighborSums[u] += uv;
            graph._vertexAllEdgeSums[u] += uv;
        } else {
            graph._vertexAllEdgeSums[u] -= uv;
        }
        ConflictTriples::updateConflictTriplesAfterUnmerge(graph, u, v);

        onEndOperation();
    } else if (lastModification.type == GraphModification::Type::FORBID) {
        onStartOperation("unforbid");
        const auto weight = lastModification.weight;
        const auto u = lastModification.u;
        const auto v = lastModification.v;

        graph.setWeight(u, v, weight);
        graph._mergeCosts[WeightedGraph::rank(u, v)] = graph.getMergeCost(u, v) - INF_WEIGHT + std::max(weight, 0);
        if (weight > 0) {
            graph._vertexNeighborSums[u] += weight;
            graph._vertexNeighborSums[v] += weight;
            graph._vertexAllEdgeSums[u] -= INF_WEIGHT - weight - weight;
            graph._vertexAllEdgeSums[v] -= INF_WEIGHT - weight - weight;
            graph._forbidCosts[WeightedGraph::rank(u, v)] = graph._forbidCosts[WeightedGraph::rank(u, v)] + weight;
        } else {
            graph._vertexAllEdgeSums[u] -= INF_WEIGHT;
            graph._vertexAllEdgeSums[v] -= INF_WEIGHT;
        }

        // Recalculate induced costs for all relevant edges
        for (auto x : graph._vertices) {
            if (x == u || x == v) {
                continue;
            }
            const auto ux = graph.getWeight(u, x);
            const auto vx = graph.getWeight(v, x);

            if (weight > 0) {
                if (ux > 0) {
                    graph._forbidCosts[WeightedGraph::rank(v, x)] = graph.getForbidCost(v, x) + std::min(ux, weight);
                    graph._mergeCosts[WeightedGraph::rank(v, x)] = graph.getMergeCost(v, x) - ux;
                } else {
                    graph._mergeCosts[WeightedGraph::rank(v, x)] = graph.getMergeCost(v, x) + std::min(-ux, weight);
                }
                if (vx > 0) {
                    graph._forbidCosts[WeightedGraph::rank(u, x)] = graph.getForbidCost(u, x) + std::min(vx, weight);
                    graph._mergeCosts[WeightedGraph::rank(u, x)] = graph.getMergeCost(u, x) - vx;
                } else {
                    graph._mergeCosts[WeightedGraph::rank(u, x)] = graph.getMergeCost(u, x) + std::min(-vx, weight);
                }
            } else {
                if (ux > 0) {
                    graph._mergeCosts[WeightedGraph::rank(v, x)] = graph.getMergeCost(v, x) + std::min(ux, -weight) - ux;
                }
                if (vx > 0) {
                    graph._mergeCosts[WeightedGraph::rank(u, x)] = graph.getMergeCost(u, x) + std::min(vx, -weight) - vx;
                }
            }
        }
        ConflictTriples::updateConflictTriplesAfterUnforbid(graph, u, v);

        onEndOperation();
    }

    if (graph.getFailureDetectedOnIndex() > graph.getNumModifications() - 1) {
       // logln("Resetting failure to -1");
        graph._failureDetectedOnIndex = -1;
    }

    onEndOperation();
}


