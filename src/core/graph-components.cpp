#include <list>

#include "graph-components.hpp"
#include "../util/logging.hpp"
#include "../util/stopwatch.hpp"

std::list<std::list<int>> GraphComponents::findConnectedComponents(WeightedGraph &graph) {
    onStartOperation("findConnectedComponents");

    std::list<std::list<int>> components;
    if (graph._vertices.size() == 0) {
        return components;
    }
    std::list<int> openVertices;
    std::fill(graph._markedVertices.begin(), graph._markedVertices.end(), false);
    int counter = 0;

    openVertices.push_back(graph._vertices.front());
    graph._markedVertices[graph._vertices.front()] = true;

    while (counter < graph._vertices.size()) {
        std::list<int> currentComponent;
        while (!openVertices.empty()) {
            auto u = openVertices.back();
            openVertices.pop_back();

            for (auto v : graph._vertices) {
                if (!graph._markedVertices[v] && graph.getWeight(u, v) > 0) {
                    graph._markedVertices[v] = true;
                    openVertices.push_back(v);
                }
            }
            currentComponent.push_back(u);
            ++counter;
        }
        currentComponent.sort();
        components.push_back(std::move(currentComponent));
        for (auto u : graph._vertices) {
            if (!graph._markedVertices[u]) {
                openVertices.push_back(u);
                graph._markedVertices[u] = true;
                break;
            }
        }
    }

    onEndOperation();

    return std::move(components);
}

int GraphComponents::findCliqueType(WeightedGraph& graph, const std::list<int> &vertices) {
    onStartOperation("findCliqueType");

    bool hasZeroEdges = false;
    for (auto uit = vertices.begin(); uit != vertices.end(); ++uit) {
        auto u = *uit;
        for (auto vit = std::next(uit); vit != vertices.end(); ++vit) {
            auto v = *vit;
            if (u == v) {
                continue;
            }
            if (graph.getWeight(u, v) < 0) {
                onEndOperation();
                return -1;
            } else if (graph.getWeight(u, v) == 0) {
                hasZeroEdges = true;
            }
        }
    }

    onEndOperation();
    if (hasZeroEdges) {
        return 0;
    }
    return 1;
}

