#include <tuple>
#include <list>
#include <algorithm>
#include <exception>
#include <cmath>

#include "conflict-triples.hpp"
#include "../util/stopwatch.hpp"

int ConflictTriples::calculateConflictTripleLowerBound(WeightedGraph &graph) {
    onStartOperation("countConflictTriples");
    std::fill(graph._conflictTriples.begin(), graph._conflictTriples.end(), NO_CONFLICT_TRIPLE);
    int lowerBound = 0;

    onStartOperation("determineConflicts");
    std::list<std::tuple<int, int, int>> oneCostTriples;
    std::list<std::tuple<int, int, int>> twoCostTriples;
    std::list<std::tuple<int, int, int, int>> otherCostTriples;
    for (auto uit = graph._vertices.begin(); uit != graph._vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != graph._vertices.end(); ++vit) {
            const auto v = *vit;
            const auto uv = graph.getWeight(u, v);
            if (uv == 0 || (uv > 0 && graph.getMergeCost(u, v) == 0) || (uv < 0 && graph.getForbidCost(u, v) == 0)) {
                continue;
            }
            for (auto wit = std::next(vit); wit != graph._vertices.end(); ++wit) {
                const auto w = *wit;
                const auto uw = graph.getWeight(u, w);
                const auto vw = graph.getWeight(v, w);
                if (uw == 0 || vw == 0) {
                    continue;
                }

                if (uv > 0) {
                    if (uw > 0 && vw < 0) {
                        const auto minCost = std::min(std::min(uv, uw), -vw);
                        if (minCost == 1) {
                            oneCostTriples.emplace_back(u, v, w);
                        } else if (minCost == 2) {
                            twoCostTriples.emplace_back(u, v, w);
                        } else {
                            otherCostTriples.emplace_back(-minCost, u, v, w);
                        }
                    } else if (vw > 0 && uw < 0) {
                        const auto minCost = std::min(std::min(uv, vw), -uw);
                        if (minCost == 1) {
                            oneCostTriples.emplace_back(u, v, w);
                        } else if (minCost == 2) {
                            twoCostTriples.emplace_back(u, v, w);
                        } else {
                            otherCostTriples.emplace_back(-minCost, u, v, w);
                        }
                    }
                } else if (uw > 0 && vw > 0) {
                    const auto minCost = std::min(std::min(uw, vw), -uv);
                    if (minCost == 1) {
                        oneCostTriples.emplace_back(u, v, w);
                    } else if (minCost == 2) {
                        twoCostTriples.emplace_back(u, v, w);
                    } else {
                        otherCostTriples.emplace_back(-minCost, u, v, w);
                    }
                }
            }
        }
    }
    onEndOperation();

    onStartOperation("sortConflicts");
    otherCostTriples.sort();
    onEndOperation();

    onStartOperation("countMinimumCost");
    for (auto& conflict : otherCostTriples) {
        auto minCost = -std::get<0>(conflict);
        const auto u = std::get<1>(conflict);
        const auto v = std::get<2>(conflict);
        const auto w = std::get<3>(conflict);
        if (graph._conflictTriples[WeightedGraph::rank(u, v)] == NO_CONFLICT_TRIPLE &&
            graph._conflictTriples[WeightedGraph::rank(u, w)] == NO_CONFLICT_TRIPLE &&
            graph._conflictTriples[WeightedGraph::rank(v, w)] == NO_CONFLICT_TRIPLE) {
            const ConflictTriple triple = {
                    static_cast<short>(u),
                    static_cast<short>(v),
                    static_cast<short>(w),
                    static_cast<short>(minCost),
            };
            lowerBound += minCost;
            graph._conflictTriples[WeightedGraph::rank(u, v)] = triple;
            graph._conflictTriples[WeightedGraph::rank(u, w)] = triple;
            graph._conflictTriples[WeightedGraph::rank(v, w)] = triple;
        }
    }
    for (auto& conflict : twoCostTriples) {
        auto minCost = 2;
        const auto u = std::get<0>(conflict);
        const auto v = std::get<1>(conflict);
        const auto w = std::get<2>(conflict);
        if (graph._conflictTriples[WeightedGraph::rank(u, v)] == NO_CONFLICT_TRIPLE &&
            graph._conflictTriples[WeightedGraph::rank(u, w)] == NO_CONFLICT_TRIPLE &&
            graph._conflictTriples[WeightedGraph::rank(v, w)] == NO_CONFLICT_TRIPLE) {
            const ConflictTriple triple = {
                    static_cast<short>(u),
                    static_cast<short>(v),
                    static_cast<short>(w),
                    static_cast<short>(minCost),
            };
            lowerBound += minCost;
            graph._conflictTriples[WeightedGraph::rank(u, v)] = triple;
            graph._conflictTriples[WeightedGraph::rank(u, w)] = triple;
            graph._conflictTriples[WeightedGraph::rank(v, w)] = triple;
        }
    }
    for (auto& conflict : oneCostTriples) {
        auto minCost = 1;
        const auto u = std::get<0>(conflict);
        const auto v = std::get<1>(conflict);
        const auto w = std::get<2>(conflict);
        if (graph._conflictTriples[WeightedGraph::rank(u, v)] == NO_CONFLICT_TRIPLE &&
            graph._conflictTriples[WeightedGraph::rank(u, w)] == NO_CONFLICT_TRIPLE &&
            graph._conflictTriples[WeightedGraph::rank(v, w)] == NO_CONFLICT_TRIPLE) {
            const ConflictTriple triple = {
                    static_cast<short>(u),
                    static_cast<short>(v),
                    static_cast<short>(w),
                    static_cast<short>(minCost),
            };
            lowerBound += minCost;
            graph._conflictTriples[WeightedGraph::rank(u, v)] = triple;
            graph._conflictTriples[WeightedGraph::rank(u, w)] = triple;
            graph._conflictTriples[WeightedGraph::rank(v, w)] = triple;
        }
    }
    onEndOperation();

    onEndOperation();
    graph._totalDisjointConflictLowerBound = lowerBound;
    return lowerBound;
}

short ConflictTriples::getResolveCost(WeightedGraph &graph, int u, int v, int w) {
    const auto uv = graph.getWeight(u, v);
    const auto uw = graph.getWeight(u, w);
    const auto vw = graph.getWeight(v, w);
    auto intCost = 0;

    if (uv > 0) {
        if (uw > 0 && vw < 0) {
            intCost = std::min(std::min(uv, uw), -vw);
        } else if (uw < 0 && vw > 0) {
            intCost = std::min(std::min(uv, vw), -uw);
        }
    } else if (uv < 0 && uw > 0 && vw > 0) {
        intCost = std::min(std::min(uw, vw), -uv);
    }

#ifdef DEBUG
        if (intCost > 32767) {
            logln("Got resolve cost ", intCost, " with ", uv, ", ", uw, " and ", vw);
            graph.printUnweighted();
            throw std::overflow_error("Cost to resolve a conflict triple is out of bounds for shorts!");
        }
#endif
    return static_cast<short>(std::min(intCost, 32767));
}

short ConflictTriples::getCurrentConflictCurrentCost(WeightedGraph &graph, int u, int v) {
    const auto conflict = graph._conflictTriples[WeightedGraph::rank(u, v)];
    if (conflict == NO_CONFLICT_TRIPLE) {
        return 0;
    }
    return getResolveCost(graph, conflict.u, conflict.v, conflict.w);
}

std::list<std::pair<int, int>> getConflictEdges(const ConflictTriple& conflict1,
                                                const ConflictTriple& conflict2,
                                                const ConflictTriple& conflict3) {
    std::list<std::pair<int, int>> edges;
    if (conflict1 != NO_CONFLICT_TRIPLE) {
        edges.emplace_back(conflict1.u, conflict1.v);
        edges.emplace_back(conflict1.u, conflict1.w);
        edges.emplace_back(conflict1.v, conflict1.w);
    }
    if (conflict2 != NO_CONFLICT_TRIPLE) {
        edges.emplace_back(conflict2.u, conflict2.v);
        edges.emplace_back(conflict2.u, conflict2.w);
        edges.emplace_back(conflict2.v, conflict2.w);
    }
    if (conflict3 != NO_CONFLICT_TRIPLE) {
        edges.emplace_back(conflict3.u, conflict3.v);
        edges.emplace_back(conflict3.u, conflict3.w);
        edges.emplace_back(conflict3.v, conflict3.w);
    }
    return std::move(edges);
}

void ConflictTriples::removeConflict(WeightedGraph &graph, const ConflictTriple &triple, bool queueEdges) {
    if (triple == NO_CONFLICT_TRIPLE) {
        return;
    }
    const auto u = triple.u;
    const auto v = triple.v;
    const auto w = triple.w;
    graph._totalDisjointConflictLowerBound -= triple.resolveCost;
    graph._conflictTriples[WeightedGraph::rank(u, v)] = NO_CONFLICT_TRIPLE;
    graph._conflictTriples[WeightedGraph::rank(u, w)] = NO_CONFLICT_TRIPLE;
    graph._conflictTriples[WeightedGraph::rank(v, w)] = NO_CONFLICT_TRIPLE;
    if (queueEdges) {
        graph._openEdges.emplace_back(u, v);
        graph._openEdges.emplace_back(u, w);
        graph._openEdges.emplace_back(v, w);
    }
}

inline int calculateClawCost(int uv, int up, int uq, int vp, int vq, int pq) {
    int cost0 = -vp - vq - pq;
    int cost1 = uv + up;
    int cost2 = uv + uq;
    int cost3 = up + uq;
    int cost4 = up - vq;
    int cost5 = uq - vp;
    int cost6 = uv - pq;
    return std::min({cost0, cost1, cost2, cost3, cost4, cost5, cost6});
}

template<typename T>
inline void emplaceStructure(std::vector<std::list<T>>& fixedCostStructures,
                             std::list<std::pair<int, T>>& variableCostStructs,
                             int cost,
                             T instance) {
    if (cost > fixedCostStructures.size()) {
        variableCostStructs.emplace_back(-cost, std::move(instance));
    } else if (cost > 0) {
        fixedCostStructures[cost - 1].push_back(std::move(instance));
    }
}

int ConflictTriples::clawConflictBound(WeightedGraph &graph) {
    onStartOperation("clawConflictBound");
    std::vector<std::list<claw_tuple_t>> fixedCostClaws = std::vector<std::list<claw_tuple_t>>(32);
    std::list<std::pair<int, claw_tuple_t >> variableCostClaws;
    std::vector<std::list<path_tuple_t>> fixedCostPaths = std::vector<std::list<path_tuple_t>>(16);
    std::list<std::pair<int, path_tuple_t >> variableCostPaths;

    onStartOperation("findPotentialEntries");
    for (auto uit = graph._vertices.begin(); uit != graph._vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != graph._vertices.end(); ++vit) {
            const auto v = *vit;
            const auto uv = graph.getWeight(u, v);
            if (uv <= 0) {
                continue;
            }
            std::list<int> uClaws;
            std::list<int> vClaws;
            for (auto w : graph._vertices) {
                if (w == u || w == v) {
                    continue;
                }
                const auto uw = graph.getWeight(u, w);
                const auto vw = graph.getWeight(v, w);
                if (uw > 0 && vw < 0) {
                    uClaws.push_front(w);
                    const auto cost = std::min({uv, uw, -vw});
                    emplaceStructure<path_tuple_t>(fixedCostPaths, variableCostPaths, cost,
                            std::move(std::make_tuple(u, v, w)));
                } else if (uw < 0 && vw > 0) {
                    vClaws.push_front(w);
                    const auto cost = std::min({uv, -uw, vw});
                    emplaceStructure<path_tuple_t>(fixedCostPaths, variableCostPaths, cost,
                            std::move(std::make_tuple(u, v, w)));
                }
            }
            if (uClaws.size() > 1) {
                for (auto pit = uClaws.begin(); pit != uClaws.end(); ++pit) {
                    const auto p = *pit;
                    for (auto qit = std::next(pit); qit != uClaws.end(); ++qit) {
                        const auto q = *qit;
                        if (graph.getWeight(p, q) >= 0) {
                            continue;
                        }
                        const auto tuple = std::make_tuple(u, v, p, q);
                        const auto cost = calculateClawCost(
                                graph.getWeight(u, v), graph.getWeight(u, p), graph.getWeight(u, q),
                                graph.getWeight(v, p), graph.getWeight(v, q), graph.getWeight(p, q)
                        );
                        emplaceStructure<claw_tuple_t>(fixedCostClaws, variableCostClaws, cost, tuple);
                    }
                }
            }
            if (vClaws.size() > 1) {
                for (auto pit = vClaws.begin(); pit != vClaws.end(); ++pit) {
                    const auto p = *pit;
                    for (auto qit = std::next(pit); qit != vClaws.end(); ++qit) {
                        const auto q = *qit;
                        if (graph.getWeight(p, q) >= 0) {
                            continue;
                        }
                        const auto tuple = std::make_tuple(v, u, p, q);
                        const auto cost = calculateClawCost(
                                graph.getWeight(u, v), graph.getWeight(v, p), graph.getWeight(v, q),
                                graph.getWeight(u, p), graph.getWeight(u, q), graph.getWeight(p, q)
                        );
                        emplaceStructure<claw_tuple_t>(fixedCostClaws, variableCostClaws, cost, tuple);
                    }
                }
            }
        }
    }
    onEndOperation();
    onStartOperation("sortClaws");
    variableCostClaws.sort();
    onEndOperation();
    onStartOperation("sortPaths");
    variableCostPaths.sort();
    onEndOperation();
    std::fill(graph._markedEdges.begin(), graph._markedEdges.end(), false);
    std::fill(graph._edgeLowerBoundChange.begin(), graph._edgeLowerBoundChange.end(), 0);
    int totalCost = 0;
    onStartOperation("iterateClaws");
    for (auto& clawTuple : variableCostClaws) {
        const auto& claw = clawTuple.second;
        const auto cost = -clawTuple.first;
        totalCost += checkToAddClaw(graph, claw, cost);
    }
    for (auto cost = fixedCostClaws.size(); cost > 0; --cost) {
        for (auto& claw : fixedCostClaws[cost - 1]) {
            totalCost += checkToAddClaw(graph, claw, cost);
        }
    }
    onEndOperation();
    onStartOperation("iteratePaths");
    for (auto& pathTuple : variableCostPaths) {
        const auto path = pathTuple.second;
        const auto cost = -pathTuple.first;
        totalCost += checkToAddPath(graph, path, cost);
    }
    for (auto cost = fixedCostPaths.size(); cost > 0; --cost) {
        for (auto& path : fixedCostPaths[cost - 1]) {
            totalCost += checkToAddPath(graph, path, cost);
        }
    }
    onEndOperation();
    graph._totalDisjointConflictLowerBound = totalCost;
    onEndOperation();

    return totalCost;
}

void ConflictTriples::removeOverlaps(WeightedGraph &graph, const ConflictTriple &triple, bool queueEdges) {
    if (triple == NO_CONFLICT_TRIPLE) {
        return;
    }
    const auto u = triple.u;
    const auto v = triple.v;
    const auto w = triple.w;
    if (graph.getWeight(u, v) > -INF_WEIGHT / 2) {
        removeConflict(graph, graph._conflictTriples[WeightedGraph::rank(u, v)], queueEdges);
    }
    if (graph.getWeight(u, w) > -INF_WEIGHT / 2) {
        removeConflict(graph, graph._conflictTriples[WeightedGraph::rank(u, w)], queueEdges);
    }
    if (graph.getWeight(v, w) > -INF_WEIGHT / 2) {
        removeConflict(graph, graph._conflictTriples[WeightedGraph::rank(v, w)], queueEdges);
    }
}

// TODO remove useless method param
void ConflictTriples::updateConflictTriples(WeightedGraph &graph,
                                            std::list<std::pair<int, int>> edges) {
    return;
    graph._openEdges = edges;
    for (auto& edge : graph._openEdges) {
        auto currentConflict = graph._conflictTriples[WeightedGraph::rank(edge.first, edge.second)];
        removeConflict(graph, currentConflict, false);
    }

    while (!graph._openEdges.empty()) {
        auto edge = graph._openEdges.front();
        graph._openEdges.pop_front();

        const auto u = edge.first;
        const auto v = edge.second;
        auto currentConflict = graph._conflictTriples[WeightedGraph::rank(u, v)];
        auto bestCost = getCurrentConflictCurrentCost(graph, u, v);
        // It the current conflict has changed in a meaningful way, remove it. It will be re-found
        if (currentConflict != NO_CONFLICT_TRIPLE
            && bestCost < currentConflict.resolveCost) {
            removeConflict(graph, currentConflict, false);
        }
        auto bestP = -1;

        if (graph.getWeight(u, v) != 0) {
            for (auto p : graph._vertices) {
                if (u == p || v == p) {
                    continue;
                }
                // TODO can probably be optimized by inlining stuff
                const auto minCost = getResolveCost(graph, u, v, p);
                const auto currentUp = getCurrentConflictCurrentCost(graph, u, p);
                const auto currentVp = getCurrentConflictCurrentCost(graph, v, p);

                if (minCost > currentUp && minCost > currentVp && minCost > bestCost) {
                    bestCost = minCost;
                    bestP = p;
                }
            }
        }

        // If we found a conflict that would be an upgrade in comparison to the current conflicts
        if (bestP != -1) {
            const auto newConflict = ConflictTriple(u, v, bestP, bestCost);
            removeOverlaps(graph, newConflict, true);

            graph._conflictTriples[WeightedGraph::rank(u, v)] = newConflict;
            graph._conflictTriples[WeightedGraph::rank(u, bestP)] = newConflict;
            graph._conflictTriples[WeightedGraph::rank(v, bestP)] = newConflict;
            graph._totalDisjointConflictLowerBound += newConflict.resolveCost;
        }
    }
}

void ConflictTriples::debugValidate(WeightedGraph &graph, std::string operation) {
    for (auto& triplet : graph._conflictTriples) {
        if (triplet != NO_CONFLICT_TRIPLE) {
            const auto u = triplet.u;
            const auto v = triplet.v;
            const auto w = triplet.w;
            const auto& one = graph._conflictTriples[WeightedGraph::rank(u, v)];
            const auto& two = graph._conflictTriples[WeightedGraph::rank(u, w)];
            const auto& three = graph._conflictTriples[WeightedGraph::rank(v, w)];
            if (one != two || two != three || one != three) {
                logln("Failure in ", operation);
                logln("  {", u, ",", v, "} => ", one.toString());
                logln("  {", u, ",", w, "} => ", two.toString());
                logln("  {", v, ",", w, "} => ", three.toString());
                throw std::invalid_argument("ERRROROROROROROA");
            }
        }
    }
}

void ConflictTriples::debugPrint(WeightedGraph &graph) {
    for (auto& triplet : graph._conflictTriples) {
        if (triplet != NO_CONFLICT_TRIPLE) {
            logln(" >>> Conflict: ", triplet.toString(), " => ", triplet.resolveCost);
        }
    }
}

void ConflictTriples::updateConflictTriplesAfterMerge(WeightedGraph &graph, int u, int v) {
    for (auto p : graph._vertices) {
        const auto currentConflict = graph._conflictTriples[WeightedGraph::rank(p, v)];
        if (currentConflict != NO_CONFLICT_TRIPLE) {
            const auto x = currentConflict.u;
            const auto y = currentConflict.v;
            const auto z = currentConflict.w;
            graph._conflictTriples[WeightedGraph::rank(x, y)] = NO_CONFLICT_TRIPLE;
            graph._conflictTriples[WeightedGraph::rank(x, z)] = NO_CONFLICT_TRIPLE;
            graph._conflictTriples[WeightedGraph::rank(y, z)] = NO_CONFLICT_TRIPLE;
            graph._totalDisjointConflictLowerBound -= currentConflict.resolveCost;
        }
    }
    std::list<std::pair<int, int>> updatedEdges;
    for (auto p : graph._vertices) {
        if (u == p || v == p) {
            continue;
        }
        updatedEdges.emplace_back(u, p);
    }
    for (auto& edge : updatedEdges) {
        adjustConflictFreeEdges(graph, edge.first, edge.second, false);
    }
    updateConflictTriples(graph, std::move(updatedEdges));
#ifdef DEBUG
    debugValidate(graph, "merge");
#endif
}

void ConflictTriples::updateConflictTriplesAfterUnmerge(WeightedGraph &graph, int u, int v) {
    for (auto p : graph._vertices) {
        if (p == v) {
            continue;
        }
        const auto currentConflict = graph._conflictTriples[WeightedGraph::rank(p, v)];
        if (currentConflict != NO_CONFLICT_TRIPLE) {
            const auto x = currentConflict.u;
            const auto y = currentConflict.v;
            const auto z = currentConflict.w;
            graph._conflictTriples[WeightedGraph::rank(x, y)] = NO_CONFLICT_TRIPLE;
            graph._conflictTriples[WeightedGraph::rank(x, z)] = NO_CONFLICT_TRIPLE;
            graph._conflictTriples[WeightedGraph::rank(y, z)] = NO_CONFLICT_TRIPLE;
            graph._totalDisjointConflictLowerBound -= currentConflict.resolveCost;
        }
    }
    std::list<std::pair<int, int>> updatedEdges;
    for (auto p : graph._vertices) {
        if (u == p || v == p) {
            continue;
        }
        updatedEdges.emplace_back(v, p);
        updatedEdges.emplace_back(u, p);
    }
    updatedEdges.emplace_back(u, v);
    for (auto& edge : updatedEdges) {
        adjustConflictFreeEdges(graph, edge.first, edge.second, true);
    }
    updateConflictTriples(graph, std::move(updatedEdges));
#ifdef DEBUG
    debugValidate(graph, "unmerge");
#endif
}

void ConflictTriples::updateConflictTriplesAfterForbid(WeightedGraph &graph, int u, int v) {
    auto updatedEdges = std::list<std::pair<int, int>>(1, std::make_pair(u, v));
    for (auto& edge : updatedEdges) {
        adjustConflictFreeEdges(graph, edge.first, edge.second, false);
    }
    updateConflictTriples(graph, std::move(updatedEdges));
#ifdef DEBUG
    debugValidate(graph, "forbid");
#endif
}

void ConflictTriples::updateConflictTriplesAfterUnforbid(WeightedGraph &graph, int u, int v) {
    auto updatedEdges = std::list<std::pair<int, int>>(1, std::make_pair(u, v));
    for (auto& edge : updatedEdges) {
        adjustConflictFreeEdges(graph, edge.first, edge.second, true);
    }
    updateConflictTriples(graph, std::move(updatedEdges));
#ifdef DEBUG
    debugValidate(graph, "unforbid");
#endif
}

void ConflictTriples::initializeConflictFreeEdges(WeightedGraph &graph) {
    std::fill(graph._conflictFreeEdges.begin(), graph._conflictFreeEdges.end(), true);
    for (auto uit = graph._vertices.begin(); uit != graph._vertices.end(); ++uit) {
        const auto u = *uit;
        const auto uCount = graph._vertexCounts[u];
        for (auto vit = std::next(uit); vit != graph._vertices.end(); ++vit) {
            int uvType = 0;

            const auto v = *vit;
            const auto vCount = graph._vertexCounts[v];
            const auto rankUv = WeightedGraph::rank(u, v);
            const auto uv = graph._edgeWeights[rankUv];

            if (uv >= -INF_WEIGHT / 2 && std::abs(uv) != vCount * uCount) {
                uvType = 0;
                // Wildcard edges are always conflicts
                graph._conflictFreeEdges[rankUv] = false;
            } else if (uv > 0) {
                uvType = 1;
            } else {
                uvType = -1;
            }

            for (auto wit = std::next(vit); wit != graph._vertices.end(); ++wit) {
                int edges = 0;
                int nonEdges = 0;
                int wildcards = 0;
                if (uvType == 0) {
                    ++wildcards;
                } else if (uvType == 1) {
                    ++edges;
                } else if (uvType == -1) {
                    ++nonEdges;
                }

                const auto w = *wit;
                const auto wCount = graph._vertexCounts[w];
                const auto rankUw = WeightedGraph::rank(u, w);
                const auto rankVw = WeightedGraph::rank(v, w);
                const auto uw = graph._edgeWeights[rankUw];
                const auto vw = graph._edgeWeights[rankVw];

                if (uw >= -INF_WEIGHT / 2 && std::abs(uw) != wCount * uCount) {
                    ++wildcards;
                } else if (uw > 0) {
                    ++edges;
                } else {
                    ++nonEdges;
                }

                if (vw >= -INF_WEIGHT / 2 && std::abs(vw) != wCount * vCount) {
                    ++wildcards;
                } else if (vw > 0) {
                    ++edges;
                } else {
                    ++nonEdges;
                }

                if (nonEdges == 1 || (nonEdges == 0 && edges <= 2)) {
                    graph._conflictFreeEdges[rankUv] = false;
                    graph._conflictFreeEdges[rankUw] = false;
                    graph._conflictFreeEdges[rankVw] = false;
                }
            }
        }
    }
}

void ConflictTriples::adjustConflictFreeEdges(WeightedGraph &graph, int u, int v, bool allowNewConflicts) {
    bool hasConflict = false;
    const auto rankUv = WeightedGraph::rank(u, v);
    const auto uv = graph.getWeight(u, v);
    const auto uCount = graph._vertexCounts[u];
    const auto vCount = graph._vertexCounts[v];

    if (std::abs(uv) != uCount * vCount) {
        hasConflict = true;
    }

    if (!hasConflict) {
        for (auto p : graph._vertices) {
            if (u == p || v == p) {
                continue;
            }
            const auto pCount = graph._vertexCounts[p];
            const auto up = graph.getWeight(u, p);
            const auto vp = graph.getWeight(v, p);
            if (std::abs(up) != pCount * uCount || std::abs(vp) != pCount * vCount) {
                hasConflict = true;
                break;
            }
            if (uv > 0) {
                if (up > 0 && vp < 0) {
                    hasConflict = true;
                    break;
                } else if (up < 0 && vp > 0) {
                    hasConflict = true;
                    break;
                }
            } else if (up > 0 || vp > 0) {
                hasConflict = true;
                break;
            }
        }
    }

    if (hasConflict) {
        if (allowNewConflicts) {
            graph._conflictFreeEdges[rankUv] = false;
        }
    } else {
        if (!graph._conflictFreeEdges[rankUv]) {
            graph._checkConflictEdges.emplace_back(u, v);
        }
        graph._conflictFreeEdges[rankUv] = true;
    }
}

int ConflictTriples::getDisjointLowerBoundAfterMerge(WeightedGraph &graph, int u, int v) {
    std::list<ConflictTriple> uniqueConflicts;
    auto conflict = graph._conflictTriples[WeightedGraph::rank(u, v)];
    // TODO optimize, set and hashing for example
    if (conflict != NO_CONFLICT_TRIPLE) {
        uniqueConflicts.push_back(conflict);
    }
    for (auto p : graph._vertices) {
        // TODO calculate if the edge still remains in a conflict?
        if (p != u && p != v) {
            conflict = graph._conflictTriples[WeightedGraph::rank(u, p)];
            // TODO optimize, set and hashing for example
            if (conflict != NO_CONFLICT_TRIPLE && std::count(uniqueConflicts.begin(), uniqueConflicts.end(), conflict) == 0) {
                uniqueConflicts.push_back(conflict);
            }
            conflict = graph._conflictTriples[WeightedGraph::rank(v, p)];
            // TODO optimize, set and hashing for example
            if (conflict != NO_CONFLICT_TRIPLE && std::count(uniqueConflicts.begin(), uniqueConflicts.end(), conflict) == 0) {
                uniqueConflicts.push_back(conflict);
            }
        }
    }
    int sum = 0;
    for (auto& conflict : uniqueConflicts) {
        sum += conflict.resolveCost;
    }
    return graph._totalDisjointConflictLowerBound - sum;
}