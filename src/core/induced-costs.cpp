#include <cmath>

#include "induced-costs.hpp"
#include "../util/stopwatch.hpp"

int InducedCosts::calculateForbidCost(const WeightedGraph& graph, int u, int v) {
    auto cost = std::max(0, graph.getWeight(u, v));
    for (auto p : graph._vertices) {
        if (u == p || v == p) {
            continue;
        }
        const auto up = graph.getWeight(u, p);
        const auto vp = graph.getWeight(v, p);
        if (up > 0 && vp > 0) {
            cost += std::min(up, vp);
        }
    }

    return cost;
}

int InducedCosts::calculateMergeCost(const WeightedGraph& graph, int u, int v)  {
    auto cost = std::max(0, -graph.getWeight(u, v));
    for (auto p : graph._vertices) {
        if (p == u || p == v) {
            continue;
        }
        const auto up = graph.getWeight(u, p);
        const auto vp = graph.getWeight(v, p);
        // Add to cost only if p is a neighbor of exactly one of {u, v}.
        if ((up > 0 && vp <= 0) || (vp > 0 && up <= 0)) {
            cost += std::min(std::abs(up), std::abs(vp));
        }
    }

    return cost;
}

void InducedCosts::calculateAllInducedCosts(WeightedGraph &graph) {
    onStartOperation("calculateAllInducedCosts");
    for (auto uit = graph._vertices.begin(); uit != graph._vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != graph._vertices.end(); ++vit) {
            const auto v = *vit;

            // TODO we can inline these and combine the accesses to weights
            const auto forbidCost = calculateForbidCost(graph, u, v);
            const auto mergeCost = calculateMergeCost(graph, u, v);

            graph._mergeCosts[WeightedGraph::rank(u, v)] = mergeCost;
            graph._forbidCosts[WeightedGraph::rank(u, v)] = forbidCost;
        }
    }
    onEndOperation();
}

