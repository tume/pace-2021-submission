#include <stdexcept>
#include <sstream>
#include <list>

#include "graph.hpp"
#include "../util/logging.hpp"
#include "../util/stopwatch.hpp"
#include "induced-costs.hpp"
#include "edge-operations.hpp"

void WeightedGraph::calculateAllWeightSums() {
    onStartOperation("calculateAllWeightSums");
    // Zero out any previous values
    for (auto uit = _vertices.begin(); uit != _vertices.end(); ++uit) {
        const auto u = *uit;
        _vertexNeighborSums[u] = 0;
        _vertexAllEdgeSums[u] = 0;
    }

    for (auto uit = _vertices.begin(); uit != _vertices.end(); ++uit) {
        const auto u = *uit;
        for (auto vit = std::next(uit); vit != _vertices.end(); ++vit) {
            const auto v = *vit;
            const auto weight = getWeight(u, v);

            if (weight > 0) {
                _vertexNeighborSums[u] += weight;
                _vertexNeighborSums[v] += weight;
                _vertexAllEdgeSums[u] += weight;
                _vertexAllEdgeSums[v] += weight;
            } else {
                _vertexAllEdgeSums[u] -= weight;
                _vertexAllEdgeSums[v] -= weight;
            }
        }
    }
    onEndOperation();
}

WeightedGraph WeightedGraph::fromStdin() {
    std::string line;

    int numVertices = -1;
    int remainingEdges = 0;

    while (std::getline(std::cin, line)) {
        if (line[0] == 'c') {
            continue;
        }
        numVertices = std::stoi(line.substr(6, line.substr(6).find(' ')));
        remainingEdges = std::stoi(line.substr(line.substr(6).find(' ') + 6));
        break;
    }

    if (numVertices == -1) {
        throw std::invalid_argument("Malformed input: could not determine number of vertices");
    }

    WeightedGraph graph(numVertices);

    while (std::getline(std::cin, line)) {
        if (line[0] == 'c') {
            continue;
        }
        if (remainingEdges <= 0) {
            throw std::invalid_argument("Malformed input: more edges than defined on first row");
        }

        auto splitIndex = line.find(' ');
        auto u = std::stoi(line.substr(0, splitIndex)) - 1;
        auto v = std::stoi(line.substr(splitIndex, line.length())) - 1;
        if (u < 0 || v < 0 || u >= numVertices || v > numVertices) {
            throw std::invalid_argument("Malformed input: a vertex is out of bounds");
        }
        graph.setWeight(u, v, 1);
        remainingEdges--;
    }

    if (remainingEdges > 0) {
        throw std::invalid_argument("Malformed input: fewer edges than defined on first row");
    }

    return graph;
}

WeightedGraph WeightedGraph::fromString(const std::string definition) {
    std::istringstream iss(definition);
    std::string line;

    int numVertices = -1;
    int remainingEdges = 0;

    while (std::getline(iss, line)) {
        if (line[0] == 'c') {
            continue;
        }
        numVertices = std::stoi(line.substr(6, line.substr(6).find(' ')));
        remainingEdges = std::stoi(line.substr(line.substr(6).find(' ') + 6));
        break;
    }

    if (numVertices == -1) {
        throw std::invalid_argument("Malformed input: could not determine number of vertices");
    }

    WeightedGraph graph(numVertices);

    while (std::getline(iss, line)) {
        if (line[0] == 'c') {
            continue;
        }
        if (remainingEdges <= 0) {
            throw std::invalid_argument("Malformed input: more edges than defined on first row");
        }

        auto splitIndex = line.find(' ');
        auto u = std::stoi(line.substr(0, splitIndex)) - 1;
        auto v = std::stoi(line.substr(splitIndex, line.length())) - 1;
        if (u < 0 || v < 0 || u >= numVertices || v > numVertices) {
            throw std::invalid_argument("Malformed input: a vertex is out of bounds");
        }
        graph.setWeight(u, v, 1);
        remainingEdges--;
    }

    if (remainingEdges > 0) {
        throw std::invalid_argument("Malformed input: fewer edges than defined on first row");
    }

    return graph;
}

std::vector<int> WeightedGraph::getAllWeights() const {
    return _edgeWeights;
}

void WeightedGraph::setAllWeights(const std::vector<int> &weights) {
    this->_edgeWeights = weights;
}

void WeightedGraph::setVertices(const std::list<int> &vertices) {
    this->_vertices = vertices;
}

void WeightedGraph::copyModifications(std::list<GraphModification> &into) const {
    onStartOperation("copyModifications");
    into.clear();
    for (auto mod : _modificationHistory) {
        into.push_back({ mod.type, mod.u, mod.v, mod.weight, mod.accumulatedCost });
    }
    onEndOperation();
}

void WeightedGraph::setModifications(std::list<GraphModification> &from) {
    onStartOperation("setModifications");
    _modificationHistory = std::list<GraphModification>(from);
    onEndOperation();
}

void padLog(std::string value, int requiredPad) {
    log(value);
    for (auto pad = value.length(); pad < requiredPad; pad++) {
        log(" ");
    }
}

void WeightedGraph::printMarkedEdges() const {
#ifdef DEBUG
    if (_vertices.size() > 120) {
        logln("Skipping print due to too many vertices");
        return;
    }
    auto maxVertex = 0;
    for (auto u : getVertices()) {
        if (u > maxVertex) {
            maxVertex = u;
        }
    }
    auto colWidth = 2;
    if (_vertices.size() <= 60) {
        colWidth = std::to_string(maxVertex + 1).length() + 1;
    }
    logln("Printing graph, column width ", colWidth, "!");
    padLog("", colWidth);
    for (auto x : getVertices()) {
        if (_vertices.size() > 60) {
            padLog("?", colWidth);
        } else {
            padLog(std::to_string(x + 1), colWidth);
        }
    }
    logln("");

    for (auto y : getVertices()) {
        padLog(std::to_string(y + 1), std::to_string(maxVertex + 1).length() + 1);
        for (auto x : getVertices()) {
            if (x == y) {
                padLog(" ", colWidth);
            } else if (getWeight(x, y) > 0) {
                if (isMarked(x, y)) {
                    padLog("M", colWidth);
                } else {
                    padLog("X", colWidth);
                }
            } else {
                if (isMarked(x, y)) {
                    padLog("-", colWidth);
                } else {
                    padLog(".", colWidth);
                }
            }
        }
        logln("");
    }
#endif
}

void WeightedGraph::printUnweighted() const {
#ifdef DEBUG
    auto maxVertex = _maxVertexIndex;
    auto colWidth = 1;
    if (maxVertex <= 60) {
        colWidth = std::to_string(maxVertex + 1).length() + 1;
    }
    logln("Printing graph, column width ", colWidth, "!");
    padLog("", colWidth);
    for (auto x : getVertices()) {
        if (maxVertex > 60) {
            padLog("?", colWidth);
        } else {
            padLog(std::to_string(x + 1), colWidth);
        }
    }
    logln("");

    for (auto y : getVertices()) {
        padLog(std::to_string(y + 1), std::to_string(maxVertex + 1).length() + 1);
        for (auto x : getVertices()) {
            if (x == y) {
                padLog(" ", colWidth);
            } else if (getWeight(x, y) > 0) {
                if (_conflictFreeEdges[rank(x, y)]) {
                    padLog("Y", colWidth);
                } else {
                    padLog("X", colWidth);
                }
            } else if (getWeight(x, y) == 0) {
                padLog("*", colWidth);
            } else if (getWeight(x, y) < -INF_WEIGHT / 2) {
                padLog("-", colWidth);
            } else {
                if (_conflictFreeEdges[rank(x, y)]) {
                    padLog(",", colWidth);
                } else {
                    padLog(".", colWidth);
                }
            }
        }
        logln("");
    }
#endif
}

void WeightedGraph::printModifications() const {
#ifdef DEBUG
    logln("Printing modifications applied to graph:");
    auto index = 0;
    for (auto mod : _modificationHistory) {
        logln(" ", ++index, ": ", mod.type, " for {", mod.u, ",", mod.v, "} with weight=", mod.weight);
    }
#endif
}
