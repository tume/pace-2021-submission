#pragma once

#include "graph.hpp"

class MinCut {
public:
    /**
     * Calculates the minimum cut value for the induced subgraph for the given graph,
     * induced by the given vertices.
     */
    static int calculateMinimumCut(WeightedGraph &graph, const std::list<int>& vertices);
private:
    /**
     * A helper function, intended to be used by `calculateMinimumCut`. Calculates the
     * minimum s-t cut for some vertices s-t, and then merges the vertices s & t.
     */
    static int calculateSomeMinSTCut(WeightedGraph& graph);

    /**
     * A special vertex-merge operation that does not update the induced costs, and
     * considers negative edges as zero-edges.
     */
    static void merge(WeightedGraph& graph, int u, int v);
};