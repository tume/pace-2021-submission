#pragma once

#include <unordered_set>
#include <algorithm>

#include "graph.hpp"

using claw_tuple_t = std::tuple<int, int, int, int>;
using path_tuple_t = std::tuple<int, int, int>;

class ConflictTriples {
public:
    /**
     * TODO
     */
    static void adjustConflictFreeEdges(WeightedGraph& graph, int u, int v, bool allowNewConflicts);

    /**
     * TODO
     */
    static void initializeConflictFreeEdges(WeightedGraph& graph);

    /**
     * TODO
     */
    static int calculateConflictTripleLowerBound(WeightedGraph& graph);

    static int clawConflictBound(WeightedGraph& graph);

    /**
     * TODO
     */
    static void updateConflictTriplesAfterMerge(WeightedGraph& graph, int u, int v);

    static void updateConflictTriplesAfterUnmerge(WeightedGraph& graph, int u, int v);

    /**
     * TODO
     */
    static void updateConflictTriplesAfterForbid(WeightedGraph& graph, int u, int v);

    static void updateConflictTriplesAfterUnforbid(WeightedGraph& graph, int u, int v);

    static void debugPrint(WeightedGraph& graph);

    static void debugValidate(WeightedGraph& graph, std::string operation);

    static inline ConflictTriple& getConflictTriple(WeightedGraph& graph, int u, int v) {
        return graph._conflictTriples[WeightedGraph::rank(u, v)];
    }

    static inline int getDisjointLowerBoundAfterForbid(WeightedGraph& graph, int u, int v) {
        if (graph.getWeight(u, v) <= 0) {
            return graph._totalDisjointConflictLowerBound;
        } else {
            const auto& triple = graph._conflictTriples[WeightedGraph::rank(u, v)];
            return graph._totalDisjointConflictLowerBound - triple.resolveCost;
        }
    }

    static int getDisjointLowerBoundAfterMerge(WeightedGraph& graph, int u, int v);
private:
    static void updateConflictTriples(WeightedGraph& graph, std::list<std::pair<int, int>> edges);

    static short getCurrentConflictCurrentCost(WeightedGraph& graph, int u, int v);

    static short getResolveCost(WeightedGraph &graph, int u, int v, int w);

    static void removeConflict(WeightedGraph &graph, const ConflictTriple& triple, bool queueEdges);

    static void removeOverlaps(WeightedGraph &graph, const ConflictTriple& triple, bool queueEdges);

    static inline int checkToAddClaw(WeightedGraph& graph, const claw_tuple_t& claw, int cost) {
        const auto u = std::get<0>(claw);
        const auto v = std::get<1>(claw);
        const auto p = std::get<2>(claw);
        const auto q = std::get<3>(claw);
        const auto rankUv = WeightedGraph::rank(u, v);
        const auto rankUp = WeightedGraph::rank(u, p);
        const auto rankUq = WeightedGraph::rank(u, q);
        const auto rankVp = WeightedGraph::rank(v, p);
        const auto rankVq = WeightedGraph::rank(v, q);
        const auto rankPq = WeightedGraph::rank(p, q);

        if (!graph._markedEdges[rankUv] && !graph._markedEdges[rankUp] && !graph._markedEdges[rankUq]
            && !graph._markedEdges[rankVp] && !graph._markedEdges[rankVq] && !graph._markedEdges[rankPq]) {
            graph._markedEdges[rankUv] = true;
            graph._markedEdges[rankUp] = true;
            graph._markedEdges[rankUq] = true;
            graph._markedEdges[rankVp] = graph._edgeWeights[rankVp] > -INF_WEIGHT / 2;
            graph._markedEdges[rankVq] = graph._edgeWeights[rankVq] > -INF_WEIGHT / 2;
            graph._markedEdges[rankPq] = graph._edgeWeights[rankPq] > -INF_WEIGHT / 2;

            const auto uv = graph._edgeWeights[rankUv];
            const auto up = graph._edgeWeights[rankUp];
            const auto uq = graph._edgeWeights[rankUq];
            const auto vp = graph._edgeWeights[rankVp];
            const auto vq = graph._edgeWeights[rankVq];
            const auto pq = graph._edgeWeights[rankPq];

            graph._edgeLowerBoundChange[rankUv] = std::min({ cost, up, uq, -pq }) - cost;
            graph._edgeLowerBoundChange[rankUp] = std::min({ cost, uv, uq, -vq }) - cost;
            graph._edgeLowerBoundChange[rankUq] = std::min({ cost, up, uv, -vq }) - cost;
            graph._edgeLowerBoundChange[rankVp] = std::min({ cost, -vq - pq, uq }) - cost;
            graph._edgeLowerBoundChange[rankVq] = std::min({ cost, -vp - pq, up }) - cost;
            graph._edgeLowerBoundChange[rankPq] = std::min({ cost, -vp - vq, uv }) - cost;

            return cost;
        }

        return 0;
    }

    static inline int checkToAddPath(WeightedGraph& graph, const path_tuple_t& path, int cost) {
        const auto u = std::get<0>(path);
        const auto v = std::get<1>(path);
        const auto w = std::get<2>(path);
        const auto rankUv = WeightedGraph::rank(u, v);
        const auto rankUw = WeightedGraph::rank(u, w);
        const auto rankVw = WeightedGraph::rank(v, w);
        if (!graph._markedEdges[rankUv] && !graph._markedEdges[rankUw] && !graph._markedEdges[rankVw]) {
            graph._markedEdges[rankUv] = graph._edgeWeights[rankUv] > -INF_WEIGHT / 2;
            graph._markedEdges[rankUw] = graph._edgeWeights[rankUw] > -INF_WEIGHT / 2;
            graph._markedEdges[rankVw] = graph._edgeWeights[rankVw] > -INF_WEIGHT / 2;

            graph._edgeLowerBoundChange[rankUv] = -cost;
            graph._edgeLowerBoundChange[rankUw] = -cost;
            graph._edgeLowerBoundChange[rankVw] = -cost;

            return cost;
        }

        return 0;
    }

};