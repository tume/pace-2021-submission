#pragma once

#include "graph.hpp"

class EdgeOperations {
public:
    /**
     * Merges the two vertices u and v. Essentially this sets the edge weights of all
     * edges incident to v to -1, and adjusts the edge weights of u accordingly.
     * Some edge modifications may be done as a result of this - this is the case when
     * there exists a vertex p that is a neighbor of either u or v, but not a neighbor
     * of both. When vertices are merged, they will have to end up with identical
     * neighborhoods, thus we will modify the edge with lower cost.
     *
     * A GraphModification entry is created from this call, and induced costs will be
     * updated to a correct state, provided they were correct before the operation.
     */
    static void merge(WeightedGraph& graph, int u, int v);

    /**
     * Forbids the edge {u,v} by assigning a large negative weight to the edge between them.
     * A GraphModification entry is created from this, and the induced costs will be updated
     * to a correct state, provided they were correct before the operation.
     */
    static void forbid(WeightedGraph& graph, int u, int v);

    /**
     * Reverses the last modification (merge or forbid) applied to this graph, and removes
     * its entry from the GraphModification history.
     */
    static void undo(WeightedGraph& graph);

    /**
     * Repeatedly invokes undo() until the number of modifications applied to the graph is
     * equal to the given target number of modifications.
     */
    static void undoBackTo(WeightedGraph& graph, int targetNumModifications);
};