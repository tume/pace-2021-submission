#include <iostream>

#include "util/logging.hpp"

#ifdef RUN_TEST_SUITE
#include "test/testsuite.hpp"
#else
#include "solver/improved-solver.hpp"
#include "core/graph.hpp"
#include "core/conflict-triples.hpp"
#include "core/induced-costs.hpp"
#include "util/stopwatch.hpp"

#endif

#ifdef RUN_TEST_SUITE
int runTestSuite() {
    int returnCode = runTests();
    logln("All tests ran, exiting main()");
    return returnCode;
}
#else
int runSolver() {
    resetStopwatch();

    logln("Reading graph from stdin");
    auto graph = WeightedGraph::fromStdin();

    if (graph.getVertices().size() == 0) {
        logln("Graph is empty, exiting");
        return 0;
    }

    InducedCosts::calculateAllInducedCosts(graph);
    graph.calculateAllWeightSums();
    //ConflictTriples::calculateConflictTripleLowerBound(graph);
    ConflictTriples::initializeConflictFreeEdges(graph);
    ConflictTriples::clawConflictBound(graph);
    auto lowerBound = graph.getTotalDisjointConflictLowerBound();
    logln("Calculated lower bound from conflict triples to ", lowerBound);

    graph.printUnweighted();

    auto solver = ImprovedSolver(graph);

    auto maxDistance = lowerBound * 16 / 14 + graph.getVertices().size();
    bool solved = false;

    while (!solved) {
        logln("Attempting a solve with distance=", maxDistance);
        if (maxDistance == 0) {
            return 0;
        }
        solver.prepare(maxDistance);
        solved = solver.recurse();
        // Increase the search tree size on failure
        maxDistance *= 2;
    }

    solver.printSolution();

    return 0;
}
#endif

int main(int argc, char **argv) {
    logln("Running with C++ version ", __cplusplus);

#ifdef RUN_TEST_SUITE
    return runTestSuite();
#endif
#ifndef RUN_TEST_SUITE
    return runSolver();
#endif
}
