#pragma once

#include "graph-tests.hpp"
#include "../util/logging.hpp"

int runTests() {
    logln("Beginning test suite");

    logln("Running graph tests");
    runGraphTests();

    return 0;
}