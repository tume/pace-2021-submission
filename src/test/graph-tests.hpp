#pragma once

#include <algorithm>

#include "test-utils.hpp"
#include "../core/graph.hpp"
#include "../core/graph-components.hpp"
#include "../core/induced-costs.hpp"
#include "../core/edge-operations.hpp"
#include "../util/logging.hpp"

void testNumVertices() {
    onTestBegin();

    auto graph = WeightedGraph(10);
    testLog("Checking that a graph initialized with 10 vertices has 10 vertices");
    assertEquals(10, graph.getVertices().size());

    auto otherGraph = WeightedGraph(5);
    testLog("Checking that a graph initialized with 5 vertices has 5 vertices");
    assertEquals(5, otherGraph.getVertices().size());

    onTestEnd();
}

void testInitialEdgeWeights() {
    onTestBegin();

    auto graph = WeightedGraph(5);

    for (auto u = 0; u < 5; ++u) {
        for (auto v = 0; v < 5; ++v) {
            if (u == v) {
                continue;
            }
            testLog("Checking that edge weights between ", u, " and ", v, " are the initial -1");
            assertEquals(-1, graph.getWeight(u, v));
        }
    }

    onTestEnd();
}

void testSetEdgeWeights() {
    onTestBegin();

    auto graph = WeightedGraph(5);
    graph.setWeight(1, 3, -2);
    graph.setWeight(0, 4, 3);

    for (auto u = 0; u < 5; ++u) {
        for (auto v = 0; v < 5; ++v) {
            if (u == v) {
                continue;
            }
            testLog("Checking edge weights between ", u, " and ", v);
            auto expected = -1;
            if ((u == 1 && v == 3) || (u == 3 && v == 1)) {
                expected = -2;
            } else if ((u == 0 && v == 4) || (u == 4 && v == 0)) {
                expected = 3;
            }
            assertEquals(expected, graph.getWeight(u, v));
        }
    }

    onTestEnd();
}

void testGraphFromString() {
    onTestBegin();

    std::string input = "c Initial comment which should be ignored\n"
                        "p cep 5 4\n"
                        "c Another comment that should be ignored\n"
                        "1 2\n"
                        "2 3\n"
                        "3 4\n"
                        "4 5\n";

    auto graph = WeightedGraph::fromString(input);

    testLog("The graph should have 5 vertices");
    assertEquals(5, graph.getVertices().size());

    int vertex = 0;
    for (auto u : graph.getVertices()) {
        testLog("The vertex number ", vertex, " should be correct");
        assertEquals(vertex++, u);
    }

    for (auto u : graph.getVertices()) {
        for (auto v : graph.getVertices()) {
            if (u == v) {
                continue;
            }
            if (u == v + 1 || v == u + 1) {
                testLog(u, " and ", v, " appear in the input, so they should have a weight of 1");
                assertEquals(1, graph.getWeight(u, v));
            } else {
                testLog(u, " and ", v, " do not appear in the input, so they should have a weight of -1");
                assertEquals(-1, graph.getWeight(u, v));
            }
        }
    }

    onTestEnd();
}

void testFindConnectedComponents_fullyConnected() {
    onTestBegin();

    std::string input = "p cep 3 2\n"
                        "1 2\n"
                        "2 3\n";
    auto graph = WeightedGraph::fromString(input);

    auto connectedComponents = GraphComponents::findConnectedComponents(graph);

    testLog("The whole graph should be connected");
    assertEquals(1, connectedComponents.size());
    auto component = connectedComponents.front();

    testLog("The connected component should have correct size");
    assertEquals(3, component.size());

    for (auto u = 0; u < 3; ++u) {
        testLog("Vertex ", u, " should appear in the component once");
        assertEquals(1, std::count(component.begin(), component.end(), u));
    }

    onTestEnd();
}

void testFindConnectedComponents_multipleComponents() {
    onTestBegin();

    std::string input = "p cep 5 3\n"
                        "1 2\n"
                        "2 3\n"
                        "4 5";
    auto graph = WeightedGraph::fromString(input);

    auto connectedComponents = GraphComponents::findConnectedComponents(graph);

    testLog("The graph should have two components");
    assertEquals(2, connectedComponents.size());
    auto bigComponent = connectedComponents.front();
    auto smallComponent = connectedComponents.back();

    // Technically the order is undefined, so better check we have it right here,
    // this will make assertions easier
    if (bigComponent.size() < smallComponent.size()) {
        auto tmp = smallComponent;
        smallComponent = bigComponent;
        bigComponent = tmp;
    }

    testLog("The large connected component should have correct size");
    assertEquals(3, bigComponent.size());

    for (auto u = 0; u < 3; ++u) {
        testLog("Vertex ", u, " should appear in the large component once");
        assertEquals(1, std::count(bigComponent.begin(), bigComponent.end(), u));
    }

    testLog("The small connected component should have correct size");
    assertEquals(2, smallComponent.size());

    for (auto u = 3; u < 5; ++u) {
        testLog("Vertex ", u, " should appear in the small component once");
        assertEquals(1, std::count(smallComponent.begin(), smallComponent.end(), u));
    }

    onTestEnd();
}

void testFindClique_strongClique() {
    onTestBegin();

    std::string input = "p cep 5 4\n"
                        "1 2\n"
                        "2 3\n"
                        "3 1\n"
                        "4 5";
    auto graph = WeightedGraph::fromString(input);

    testLog("The vertices 1,2,3 should form a clique");
    std::list<int> vertices = { 0, 1, 2 };
    auto cliqueType = GraphComponents::findCliqueType(graph, vertices);
    assertEquals(STRONG_CLIQUE, cliqueType);

    testLog("The vertices 4,5 should form a clique");
    vertices = { 3,4 };
    cliqueType = GraphComponents::findCliqueType(graph, vertices);
    assertEquals(STRONG_CLIQUE, cliqueType);

    testLog("The vertices 3,4 should not form a clique");
    vertices = { 2,3 };
    cliqueType = GraphComponents::findCliqueType(graph, vertices);
    assertEquals(NOT_CLIQUE, cliqueType);

    testLog("The vertices 1,2,3,4 should not form a clique");
    vertices = { 0,1,2,3 };
    cliqueType = GraphComponents::findCliqueType(graph, vertices);
    assertEquals(NOT_CLIQUE, cliqueType);

    onTestEnd();
}

void testFindClique_weakClique() {
    onTestBegin();

    std::string input = "p cep 5 3\n"
                        "1 2\n"
                        "2 3\n"
                        "4 5";
    auto graph = WeightedGraph::fromString(input);
    // Create a zero edge
    graph.setWeight(0, 2, 0);

    testLog("The vertices 1,2,3 should form a weak clique");
    std::list<int> vertices = { 0, 1, 2 };
    auto cliqueType = GraphComponents::findCliqueType(graph, vertices);
    assertEquals(WEAK_CLIQUE, cliqueType);

    testLog("The vertices 1,2 should still form a strong clique");
    vertices = { 0, 1 };
    cliqueType = GraphComponents::findCliqueType(graph, vertices);
    assertEquals(STRONG_CLIQUE, cliqueType);

    onTestEnd();
}

void testCalculateForbidCost() {
    onTestBegin();

    std::string input = "p cep 5 1\n"
                        "1 2\n";
    auto graph = WeightedGraph::fromString(input);

    testLog("The vertices 1,2 should have an induced forbid cost of 1");
    assertEquals(1, InducedCosts::calculateForbidCost(graph, 0, 1));

    testLog("The vertices 3,4 should have an induced forbid cost of 0");
    assertEquals(0, InducedCosts::calculateForbidCost(graph, 2, 3));

    graph.setWeight(0, 2, 3);
    testLog("The vertices 1,2 should still have an induced forbid cost of 1");
    assertEquals(1, InducedCosts::calculateForbidCost(graph, 0, 1));

    graph.setWeight(1, 2, 10);
    testLog("The vertices 1,2 should now have an induced forbid cost of 4");
    assertEquals(4, InducedCosts::calculateForbidCost(graph, 0, 1));

    onTestEnd();
}

void testCalculateMergeCost() {
    onTestBegin();

    std::string input = "p cep 5 1\n"
                        "1 2\n";
    auto graph = WeightedGraph::fromString(input);

    testLog("The vertices 1,2 should have an induced merge cost of 0");
    assertEquals(0, InducedCosts::calculateMergeCost(graph, 0, 1));

    testLog("The vertices 3,4 should have an induced merge cost of 1");
    assertEquals(1, InducedCosts::calculateMergeCost(graph, 2, 3));

    testLog("The vertices 2,3 should have an induced merge cost of 2");
    assertEquals(2, InducedCosts::calculateMergeCost(graph, 1, 2));

    onTestEnd();
}

void testMerge() {
    onTestBegin();

    std::string input = "p cep 5 4\n"
                        "1 2\n"
                        "2 3\n"
                        "1 3\n"
                        "2 4";
    auto graph = WeightedGraph::fromString(input);

    GraphOperations::merge(graph, 0, 1);

    testLog("The amount of vertices should be lowered");
    assertEquals(4, graph.getVertices().size());

    testLog("The vertex 2 should no longer exist in the graph");
    assertEquals(0, std::count(graph.getVertices().begin(), graph.getVertices().end(), 1));

    testLog("The vertex 1 should've become adjacent to 2's neighbors with the correct weight");
    assertEquals(2, graph.getWeight(0, 2));
    assertEquals(0, graph.getWeight(0, 3));

    onTestEnd();
}

void testCalculateAllInducedCosts() {
    onTestBegin();

    std::string input = "p cep 5 4\n"
                        "1 2\n"
                        "2 3\n"
                        "1 3\n"
                        "2 4";
    auto graph = WeightedGraph::fromString(input);
    InducedCosts::calculateAllInducedCosts(graph);

    testLog("The cost to merge 1 and 2 should be 1, as the only edge adding cost to it is {2,4}");
    assertEquals(1, graph.getMergeCost(0, 1));
    testLog("The cost to forbid {1,2} should be 2, as {1,3} or {2,3} must be removed in addition to {1,2}");
    assertEquals(2, graph.getForbidCost(0, 1));

    testLog("The cost to merge 1 and 4 should be 2, as it requires the edges {1,4} and {3,4}");
    assertEquals(2, graph.getMergeCost(0, 3));
    testLog("The cost to forbid {1,4} should be 1, as it requires removing {1,2} or {2,4}");
    assertEquals(1, graph.getForbidCost(0, 3));

    onTestEnd();
}

void testCalculateAllInducedCosts_almostCluster() {
    onTestBegin();

    std::string input = "p cep 5 9\n"
                        "1 3\n"
                        "1 4\n"
                        "1 5\n"
                        "2 3\n"
                        "2 4\n"
                        "2 5\n"
                        "3 4\n"
                        "3 5\n"
                        "4 5";
    auto graph = WeightedGraph::fromString(input);
    InducedCosts::calculateAllInducedCosts(graph);

    const auto vertices = graph.getVertices();
    for (auto u : vertices) {
        if (u == 0 || u == 1) {
            continue;
        }
        const auto icp0 = graph.getMergeCost(u, 0);
        const auto icf0 = graph.getForbidCost(u, 0);
        const auto icp1 = graph.getMergeCost(u, 1);
        const auto icf1 = graph.getForbidCost(u, 1);
        testLog("Edge {1,", u + 1, "} should have icp=", 1);
        assertEquals(1, icp0);
        testLog("Edge {2,", u + 1, "} should have icp=", 1);
        assertEquals(1, icp1);
        testLog("Edge {1,", u + 1, "} should have icf=", 3);
        assertEquals(3, icf0);
        testLog("Edge {2,", u + 1, "} should have icf=", 3);
        assertEquals(3, icf1);
    }

    testLog("Edge {3,4} should have icp=0");
    assertEquals(0, graph.getMergeCost(2, 3));
    testLog("Edge {3,5} should have icp=0");
    assertEquals(0, graph.getMergeCost(2, 4));
    testLog("Edge {4,5} should have icp=0");
    assertEquals(0, graph.getMergeCost(3, 4));

    testLog("Edge {3,4} should have icf=4");
    assertEquals(4, graph.getForbidCost(2, 3));
    testLog("Edge {3,5} should have icf=4");
    assertEquals(4, graph.getForbidCost(2, 4));
    testLog("Edge {4,5} should have icf=4");
    assertEquals(4, graph.getForbidCost(3, 4));

    onTestEnd();
}

void testInducedCosts_updateOnMerge() {
    onTestBegin();

    std::string input = "p cep 5 4\n"
                        "1 2\n"
                        "2 3\n"
                        "1 3\n"
                        "2 4";
    auto graph = WeightedGraph::fromString(input);
    InducedCosts::calculateAllInducedCosts(graph);

    GraphOperations::merge(graph, 0, 1);
    testLog("The cost to forbid {1,3} should be 2, as the edge should now have double weight");
    assertEquals(2, graph.getForbidCost(0, 2));

    onTestEnd();
}

void testAllWeights() {
    onTestBegin();

    auto graph = WeightedGraph(5);
    graph.setWeight(0, 2, 2);
    graph.setWeight(0, 3, 3);
    graph.setWeight(0, 4, 4);

    auto weights = graph.getAllWeights();

    testLog("Initially the weights should be what are expected");
    assertEquals(-1, weights[WeightedGraph::rank(0, 1)]);
    assertEquals(2, weights[WeightedGraph::rank(0, 2)]);
    assertEquals(3, weights[WeightedGraph::rank(0, 3)]);
    assertEquals(4, weights[WeightedGraph::rank(0, 4)]);

    graph.setWeight(0, 1, 5);
    testLog("The copy of weights should not update when the graph's weights are updated");
    assertEquals(-1, weights[WeightedGraph::rank(0, 1)]);

    weights = graph.getAllWeights();
    testLog("Calling getAllWeights again should return an updated copy");
    assertEquals(5, weights[WeightedGraph::rank(0, 1)]);

    graph.setWeight(0, 1, 8);
    testLog("The graph weights should still be updateable normally");
    assertEquals(8, graph.getWeight(0, 1));

    graph.setAllWeights(weights);
    testLog("The graphs weights should've updated correctly");
    assertEquals(5, graph.getWeight(0, 1));

    onTestEnd();
}

void testAccumulatedCost_updateOnMerge() {
    onTestBegin();

    std::string input = "p cep 5 4\n"
                        "1 2\n"
                        "2 3\n"
                        "1 3\n"
                        "2 4";
    auto graph = WeightedGraph::fromString(input);
    InducedCosts::calculateAllInducedCosts(graph);

    testLog("Initially the accumulated cost should be 0");
    assertEquals(0, graph.getAccumulatedCost());

    // Sanity check - if we've made an incorrect assumption/broken the induced cost calculation,
    // this test should fail before reporting problems in the accumulated cost
    testLog("The induced merge cost for {2,4} should be 2");
    assertEquals(2, graph.getMergeCost(1, 3));
    GraphOperations::merge(graph, 1, 3);

    testLog("After merge, the accumulated cost should've updated to 2");
    assertEquals(2, graph.getAccumulatedCost());

    onTestEnd();
}

void testAccumulatedCost_updateOnMultipleMerges() {
    onTestBegin();

    std::string input = "p cep 8 6\n"
                        "1 2\n"
                        "2 3\n"
                        "1 3\n"
                        "2 4\n"
                        "5 6\n"
                        "6 7";
    auto graph = WeightedGraph::fromString(input);
    InducedCosts::calculateAllInducedCosts(graph);

    testLog("Initially the accumulated cost should be 0");
    assertEquals(0, graph.getAccumulatedCost());

    GraphOperations::merge(graph, 1, 3);
    testLog("After merge, the accumulated cost should've updated to 2");
    assertEquals(2, graph.getAccumulatedCost());

    // The cost should be 4 instead of 2 below because merging {2,4} has doubled all weights to unconnected vertices
    testLog("The cost to merge 2 and 5 should be 4, as the vertices itself aren't connected and {2,6} must be added");
    assertEquals(4, graph.getMergeCost(1, 5));
    GraphOperations::merge(graph, 1, 5);

    testLog("The accumulated cost should have been updated");
    assertEquals(6, graph.getAccumulatedCost());

    GraphOperations::undo(graph);
    testLog("Upon undo, the accumulated cost should have reverted to the previous value");
    assertEquals(2, graph.getAccumulatedCost());

    testLog("After undo, the merged vertices 2 & 6 should be found at their correct places");
    int i = 0;
    bool found2 = false;
    bool found6 = false;
    for (auto u : graph.getVertices()) {
        ++i;
        if (i == 2) {
            found2 = true;
        } else if (i == 6) {
            found6 = true;
        }
    }
    testLog("Vertex 2 appears in its correct place");
    assertTrue(found2);
    testLog("Vertex 6 appears in its correct place");
    assertTrue(found6);

    // If the undo does not correctly update the weights, then {2,8} will have a weight of -3
    testLog("The weight of {2,8} should be correct");
    assertEquals(-2, graph.getWeight(1, 7));

    onTestEnd();
}

void testAccumulatedCost_updateOnForbid() {
    onTestBegin();

    std::string input = "p cep 5 4\n"
                        "1 2\n"
                        "2 3\n"
                        "1 3\n"
                        "2 4";
    auto graph = WeightedGraph::fromString(input);
    InducedCosts::calculateAllInducedCosts(graph);

    testLog("Initially the accumulated cost should be 0");
    assertEquals(0, graph.getAccumulatedCost());

    GraphOperations::forbid(graph, 1, 2);

    // Forbid does no other modifications to the graph, so the accumulated cost should only
    // update by the weight of the edge
    testLog("After forbid, the accumulated cost should've updated to 1");
    assertEquals(1, graph.getAccumulatedCost());

    GraphOperations::forbid(graph, 2, 3);
    testLog("Forbidding a non-edge should not have increased the accumulated cost");
    assertEquals(1, graph.getAccumulatedCost());

    GraphOperations::undo(graph);
    testLog("The original weight between {3,4} should've been restored");
    assertEquals(-1, graph.getWeight(2, 3));

    testLog("The accumulated cost should still be 1 due to the single original forbid");
    assertEquals(1, graph.getAccumulatedCost());

    onTestEnd();
}

void testUndo_complexModifications() {
    onTestBegin();

    // A problem with undoes was noted when testing with this specific graph and
    // the consequence of operations below - this test was used both to replicate
    // the issue and ensure that it does not occur again if changes are made,
    // which is why it is more complex and has less explanations that a typical
    // unit test found in this file.
    std::string input = "p cep 10 11\n"
                        "6 7\n"
                        "6 8\n"
                        "5 6\n"
                        "5 7\n"
                        "6 10\n"
                        "8 10\n"
                        "8 9\n"
                        "9 10\n"
                        "2 3\n"
                        "7 8\n"
                        "5 8";
    const auto originalGraph = WeightedGraph::fromString(input);
    auto graph = WeightedGraph::fromString(input);

    GraphOperations::forbid(graph, 4, 5);
    GraphOperations::forbid(graph, 4, 6);
    GraphOperations::forbid(graph, 4, 7);
    GraphOperations::forbid(graph, 5, 6);
    GraphOperations::forbid(graph, 5, 7);
    GraphOperations::forbid(graph, 5, 8);
    GraphOperations::forbid(graph, 5, 9);
    GraphOperations::forbid(graph, 6, 7);
    GraphOperations::undo(graph);
    GraphOperations::merge(graph, 6, 7);

    // Revert all changes and then check that the graph has reverted back to normal state.
    GraphOperations::undoBackTo(graph, 0);

    testLog("The graph should have the correct amount of vertices after everything has been undoed");
    assertEquals(originalGraph.getVertices().size(), graph.getVertices().size());
    for (auto u : graph.getVertices()) {
        for (auto v : graph.getVertices()) {
            if (u == v) {
                continue;
            }
            testLog("The weight of {", u, ",", v, "} should be that of the original graph");
            assertEquals(originalGraph.getWeight(u, v), graph.getWeight(u, v));
        }
    }

    onTestEnd();
}

void testRank() {
    onTestBegin();

    testLog("Ranks for diag matrix should be correct");
    assertEquals(0, WeightedGraph::rank(1, 0));
    assertEquals(1, WeightedGraph::rank(2, 0));
    assertEquals(2, WeightedGraph::rank(2, 1));
    assertEquals(3, WeightedGraph::rank(3, 0));
    assertEquals(4, WeightedGraph::rank(3, 1));
    assertEquals(5, WeightedGraph::rank(3, 2));
    assertEquals(6, WeightedGraph::rank(4, 0));
    assertEquals(7, WeightedGraph::rank(4, 1));
    assertEquals(8, WeightedGraph::rank(4, 2));
    assertEquals(9, WeightedGraph::rank(4, 3));
    assertEquals(10, WeightedGraph::rank(5, 0));
    assertEquals(11, WeightedGraph::rank(5, 1));
    assertEquals(12, WeightedGraph::rank(5, 2));
    assertEquals(13, WeightedGraph::rank(5, 3));
    assertEquals(14, WeightedGraph::rank(5, 4));

    testLog("Ranks should be correct even if vertices passed in reverse order");
    assertEquals(0, WeightedGraph::rank(0, 1));
    assertEquals(1, WeightedGraph::rank(0, 2));
    assertEquals(2, WeightedGraph::rank(1, 2));
    assertEquals(3, WeightedGraph::rank(0, 3));
    assertEquals(4, WeightedGraph::rank(1, 3));
    assertEquals(5, WeightedGraph::rank(2, 3));
    assertEquals(6, WeightedGraph::rank(0, 4));
    assertEquals(7, WeightedGraph::rank(1, 4));
    assertEquals(8, WeightedGraph::rank(2, 4));
    assertEquals(9, WeightedGraph::rank(3, 4));
    assertEquals(10, WeightedGraph::rank(0, 5));
    assertEquals(11, WeightedGraph::rank(1, 5));
    assertEquals(12, WeightedGraph::rank(2, 5));
    assertEquals(13, WeightedGraph::rank(3, 5));
    assertEquals(14, WeightedGraph::rank(4, 5));

    onTestEnd();
}

void runGraphTests() {
    testNumVertices();
    testInitialEdgeWeights();
    testSetEdgeWeights();
    testGraphFromString();
    testFindConnectedComponents_fullyConnected();
    testFindConnectedComponents_multipleComponents();
    testFindClique_strongClique();
    testFindClique_weakClique();
    testCalculateForbidCost();
    testCalculateMergeCost();
    testMerge();
    testCalculateAllInducedCosts();
    testCalculateAllInducedCosts_almostCluster();
    testInducedCosts_updateOnMerge();
    testAllWeights();
    testAccumulatedCost_updateOnMerge();
    testAccumulatedCost_updateOnMultipleMerges();
    testAccumulatedCost_updateOnForbid();
    testUndo_complexModifications();
    testRank();
}
