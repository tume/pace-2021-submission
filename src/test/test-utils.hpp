#pragma once

#include <iostream>
#include <stdlib.h>
#include <list>
#include <unordered_set>
#include <boost/current_function.hpp>

#include "../util/logging.hpp"

#define onTestBegin() logln("  Running ", BOOST_CURRENT_FUNCTION)
#define onTestEnd() logln("  Completed ", BOOST_CURRENT_FUNCTION)

/**
 * Use this to log messages from tests to indent the logged messages correctly
 */
#define testLog(...) logln("    ", __VA_ARGS__)

template <typename L, typename R>
void assertEquals(L expected, R actual) {
    if (expected != actual) {
        std::cerr << "      Assertion Error!\n      Expected: " << expected << "\n      Actual: " << actual << "\n";
        exit(1);
    }
}

void assertTrue(bool condition) {
    if (!condition) {
        std::cerr << "      Assertion Error!";
        exit(1);
    }
}

void assertFalse(bool condition) {
    if (condition) {
        std::cerr << "      Assertion Error!";
        exit(1);
    }
}

template <typename T>
std::unordered_set<T> toSet(std::list<T> list) {
    std::unordered_set<T> set;
    for (auto element : list) {
        set.add(element);
    }
    return set;
}

