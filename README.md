# Pace 2021 / Exact Cluster Editing solver

This repository contains an implementation of an algorithm solving the Cluster Editing optimization problem. The 
algorithm accepts input via stdin, see their input format page
[https://pacechallenge.org/2021/tracks/#input-format] for more details.

## Compiling

Generate the `Makefile` using `cmake`, then build with `make`. The `debug` argument may optionally be supplied
to create a debug build of the solver, which prints intermediary and analytics information during runtime. The project
requires C++14.